#!/usr/bin/perl
use strict;
use File::Basename;
my $dir = $ARGV[0] || die "what dir to look in?";
-d $dir || die "must be a dir";
$dir = join(" ", @ARGV);
open FOUND, "find $dir -name \"*realign.mdups.recal.bam\" |";



while(my $mdup = <FOUND>)
{
	chomp $mdup;
	my $dir = dirname($mdup);
	my $mdupMatch = $mdup;
	$mdupMatch =~ s/.realign.mdups.recal.bam$//;
	my @bams = glob("$dir/*.bam");
	for my $bam (@bams)
	{
		chomp $bam;
		#print STDERR "REGEX:\n$mdupMatch\n$bam\n";
		if(-e $bam && -e $mdup && -s $bam <= -s $mdup && $mdup ne $bam && $bam =~ /$mdupMatch.+bam$/ && $bam !~ /pbs/ && $mdup !~ /pbs/)
		{
			print STDERR "Keep $mdup\n";
			runcmd("rm $bam");
			runcmd("rm $bam.bai");
		}
		elsif(-e $bam && -e $mdup)
		{
			 #print STDERR "SKIPPING: $bam $mdup\n";
		}
	}
}
sub runcmd
{
        my $cmd=shift @_;
        my $caller=(caller(1))[3];
        print STDERR "$caller\t$cmd\n";
        system($cmd);
}
