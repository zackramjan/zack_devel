#!/usr/bin/perl
use strict;
use File::Basename;

my $dir = $ARGV[0] || die "what dir to look in?";
-d $dir || die "must be a dir";
$dir = join(" ", @ARGV);
open FOUND, "find $dir -mtime +30 -mtime -360 -name \"*.bam\" |";



while(my $tbam = <FOUND>)
{
        chomp $tbam;
        my $dirname  = dirname($tbam);
        my @bams = glob("$dirname/*.bam");
	my @toDelete = @bams;
	my @mdups = grep(/mdups/,@bams); 
	@mdups = sort { -s $b <=> -s $a } @mdups;
	@toDelete = grep(!/mdups/,@bams);
	@toDelete = grep(!/unmapped/,@toDelete);
	@toDelete = grep(!/pbs/,@toDelete);
	@toDelete = sort { -s $b <=> -s $a } @toDelete;

	if(-e $mdups[0] && -e $toDelete[0] && $mdups[0] ne $toDelete[0] && -s $mdups[0] >= -s $toDelete[0] )
	{
		print STDERR "KEEPING  $_\n" for (@mdups);
		print STDERR "DELETING $_\n" for (@toDelete);
		runcmd("rm $_") for (@toDelete);
	}

}
sub runcmd{
	my $cmd=shift @_;
	my $caller=(caller(1))[3];
	print STDERR "$caller\t$cmd\n";
	#system($cmd);
}

