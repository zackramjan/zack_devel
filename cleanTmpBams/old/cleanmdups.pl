#!/usr/bin/perl
use strict;
my $dir = $ARGV[0] || die "what dir to look in?";
-d $dir || die "must be a dir";
open FOUND, "find $dir -name \"*mdups.bam\" |";



while(my $mdup = <FOUND>)
{
	chomp $mdup;
	my $reg = $mdup;
	$reg =~ s/\.mdups//;
	if(-e $reg && -e $mdup && -s $reg <= -s $mdup && $mdup ne $reg && $mdup !~ /hpc-pbs/)
	{
		print "rm $reg\tKeep $mdup\n"; 
		system("rm $reg");
		system("rm $reg.bai");
	}
	elsif(-e $reg && -e $mdup)
	{
		print "SKIPPING: $reg $mdup\n";
	}
	#print "$reg\t$mdup\n" ; #&& -s $reg < -s $mdup && $mdup ne $reg) ;
}
