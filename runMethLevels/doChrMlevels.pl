#!/usr/bin/perl
$input = $ARGV[0];
-e $input || die "input not found";
$output = "$input\.chrM.MethLevelAverages.metric.txt";
die "output already exists" if -e $output;

$cmd = "/home/uec-00/shared/production/software/perl_utils_usc/bissnp_trinuc_sample.pl $output $input /home/uec-00/shared/production/genomes/hg19_rCRSchrm/hg19_rCRSchrm.fa chrM";

system($cmd);
