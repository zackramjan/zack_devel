#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";


$dir = $ARGV[0] || die "specify a dir to process";
$library = $ARGV[1] || die "specify a dir to process";
$sample = $ARGV[2] || die "specify a dir to process";

-d $dir || die "dir not found";

@files = glob("$dir/*map.bam");

foreach $file (@files)
{
        my $fullpath = File::Spec->rel2abs( $file ) ;
        my $filebase = basename($file);
        my $date = `date`;
        chomp $date;

	$file =~ /(\w+)_(\w+)_s_(\d)\.map\.bam/;
	my $fc = $2;
	my $ln = $3;
	print <<EOF
<RUN center_name="USC_JHU" alias="$library\.$fc\.$ln" run_center="USC_JHU" >
  <EXPERIMENT_REF refname="$sample\.$library" />
  <RUN_ATTRIBUTES>
    <RUN_ATTRIBUTE>
      <TAG>Illumina GAII</TAG>
      <VALUE>yes</VALUE>
    </RUN_ATTRIBUTE>
   </RUN_ATTRIBUTES>
</RUN>
EOF

}

