#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";


$dir = $ARGV[0] || die "specify a dir to process";
$library = $ARGV[1] || die "specify a dir to process";
$sample = $ARGV[2] || die "specify a dir to process";

-d $dir || die "dir not found";

@files = glob("$dir/*map.bam");

	print <<EOF
<EXPERIMENT center_name="USC_JHU" alias="$sample\.$library">
  <TITLE>High Throughput bisulfite sequencing of $sample\.$library</TITLE>
  <STUDY_REF accession="UNKNOWN" refcenter="UNKNOWN" refname="UKNOWN" />
  <DESIGN>
    <DESIGN_DESCRIPTION>High Throughput bisulfite sequencing of $sample\.$library</DESIGN_DESCRIPTION>
    <SAMPLE_DESCRIPTOR accession="UNKNOWN" refcenter="UNKNOWN" refname="UNKNOWN" />
    <LIBRARY_DESCRIPTOR>
      <LIBRARY_NAME>$library</LIBRARY_NAME>
      <LIBRARY_STRATEGY>WGS</LIBRARY_STRATEGY>
      <LIBRARY_SOURCE>GENOMIC</LIBRARY_SOURCE>
      <LIBRARY_SELECTION>RANDOM</LIBRARY_SELECTION>
      <LIBRARY_LAYOUT>
        <SINGLE/>
      </LIBRARY_LAYOUT>
    </LIBRARY_DESCRIPTOR>
    <SPOT_DESCRIPTOR>
      <SPOT_DECODE_SPEC>
        <READ_SPEC>
          <READ_INDEX>0</READ_INDEX>
          <READ_LABEL>forward</READ_LABEL>
          <READ_CLASS>Application Read</READ_CLASS>
          <READ_TYPE>Forward</READ_TYPE>
          <BASE_COORD>1</BASE_COORD>
        </READ_SPEC>
      </SPOT_DECODE_SPEC>
    </SPOT_DESCRIPTOR>
  </DESIGN>
  <PLATFORM>
    <ILLUMINA>
      <INSTRUMENT_MODEL>Illumina Genome Analyzer II</INSTRUMENT_MODEL>
    </ILLUMINA>
  </PLATFORM>
  <PROCESSING>
    <BASE_CALLS>
      <SEQUENCE_SPACE>Base Space</SEQUENCE_SPACE>
      <BASE_CALLER>Bustard (unknown version)</BASE_CALLER>
    </BASE_CALLS>
    <QUALITY_SCORES>
      <QUALITY_SCORER>Bustard (unknown version)</QUALITY_SCORER>
    </QUALITY_SCORES>
  </PROCESSING>
 
</EXPERIMENT>
EOF


