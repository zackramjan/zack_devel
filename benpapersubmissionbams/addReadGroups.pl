#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";


$dir = $ARGV[0] || die "specify a dir to process";
$library = $ARGV[1] || die "specify a dir to process";
$sample = $ARGV[2] || die "specify a dir to process";

-d $dir || die "dir not found";

@files = glob("$dir/*map.bam");

foreach $file (@files)
{
        my $fullpath = File::Spec->rel2abs( $file ) ;
        my $filebase = basename($file);
        my $date = `date`;
        chomp $date;

	$file =~ /(\w+)_(\w+)_s_(\d)\.map\.bam/;
	my $fc = $2;
	my $ln = $3;
	push @cmds, "$JAVA -Xmx12g -jar $PICARD/AddOrReplaceReadGroups.jar VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate MAX_RECORDS_IN_RAM=3000000 INPUT='$file' OUTPUT='with_rg_$filebase' RGID='$fc\.$ln' RGLB='$library' RGPL='illumina GAII' RGPU='$fc\.$ln' RGSM='$sample' RGCN='USC EPIGENOME CENTER' RGDS='from file $fullpath on $date'\n";

}

#create pbs files
foreach $i (0..$#cmds)
{
	my $header = "#PBS -q laird\n";
	$header .= "#PBS -l walltime=200:00:00\n";
	$header .= "#PBS -l nodes=1:ppn=8:quadcore\n";
	$header .= "cd $pwd\n";
	my $outfileName = "fixBam.$i.sh";
	open(OUT, ">$outfileName");
	print OUT $header;
	print OUT $cmds[$i];
	close OUT;
	system("qsub $outfileName");

}


