#!/usr/bin/perl


open(IN, $ARGV[0]);
$n =1;
$total = 5000000;
$mapped = 0;
while($line = <IN>)
{
	if($n==1)
	{
		$line =~ /(^\d+)/ ;
		$total = $1;
		$total = 5000000 if $total == 0 || !$total;
	}
	if($n==3)
	{
		$line =~ /(^\d+)/ ;
		$mapped = $1;
		$mapped = 0 if !$mapped;
	}
	$n++;
}

$fraction = 100.0 * $mapped / $total; 
$genome = "mm9_unmasked_q0" if $ARGV[0] =~ /mm9/;
$genome = "rn4_q0" if $ARGV[0] =~ /rn4/;
$genome = "sacCer1_q0" if $ARGV[0] =~ /sacCer1/;
$genome = "EcoliIHE3034_q0" if $ARGV[0] =~ /Ecoli/;
$genome = "phi_plus_SNPs_q0" if $ARGV[0] =~ /phi_plus/;
$genome = "tair8.pluscontam_q0" if $ARGV[0] =~ /tair8/;
$genome = "hg19_q0" if $ARGV[0] =~ /\.hg1/;


print "<$genome>$fraction</$genome>\n" if $genome;

