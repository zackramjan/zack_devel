#!/usr/bin/perl
use strict;
use Data::Dumper;
use File::Temp qw/ tempfile tempdir /;
use MIME::Base64;

#SETTINGS ,CHANGE THESE AS NEEDED FOR YOUR ENVIRONMENT

#AD SETTINGS
my $ADSERVER = "ldap://vai.org";
my $ADBIND = "'CN=Lookup\\, LDAP,OU=VAI Admin & Service Accounts,DC=vai,DC=org'";
my $ADBINDPASS = "aZ48905Spo";
my $ADBASE = "OU=VARI,DC=vai,DC=org";
my $ADUSERFILTER = "'(&(objectClass=person)(mail=*)(sAMAccountName=*))'";
my $ADUSERATTRIBS = "sAMAccountName objectSid mail mailNickName givenName sn employeeID employeeNumber ADSRC";
my $ADGROUPFILTER = "'(&(objectClass=group))'";
my $ADGROUPATTRIBS = "cn member objectSid";
my $ADIDOFFSET = 1351800000;

#LDAP
my $LDAPSERVER = "ldap://localhost";
my $LDAPBIND = "'cn=admin,dc=unix,dc=vai,dc=org'";
my $LDAPBASE = "dc=unix,dc=vai,dc=org";
my $LDAPFILTER = "'(objectClass=posixaccount)'";
my $LDAPPASS = "PADL45so";
my $LDAPGROUPDN = "ou=groups,dc=unix,dc=vai,dc=org";

#GLOBALS
my  @warnings; my %ids; my %groups, my %ADCNToLogin; my $LOG; 
my $DEBUG_LEVEL = 4;
my $TIME_NOW = time;
chdir("/opt/ldap-ad-gateway");
runcmd("/sbin/slapcat | gzip > slapcat_backup.$TIME_NOW\.gz");
open($LOG, ">ADimport.log.$TIME_NOW");
my $USERMAP = "usermap.txt";
my $GROUPMAP = "groupmap.txt";
unlink $USERMAP;
unlink $GROUPMAP;

insertLDIF(processUsers());
logMSG("$_\t$ids{$_}",3) for sort {$a <=> $b} keys %ids;
processOUGroups();
processADGroups();
logMSG($_,2) for sort @warnings;

sub processOUGroups
{
	#sync groups
	for my $g (keys %groups)
	{
		my @existingMembers = addGroupToLDAP($g);
		modifyGroup($g,\@existingMembers,\@{$groups{$g}});
	}
}

sub processADGroups
{
	my $results = getADEntries($ADGROUPFILTER,$ADGROUPATTRIBS);
	for my $r (@{$results})
	{
		my $group = $r->{cn};
		
		#skip groups that are numeric digits
		next if $group =~ /^\d+/;
		$group = cleanName($group);
		
		my @members = split(/\|/,$r->{member});
		@members = map {$ADCNToLogin{$_}} @members;
		@members = grep /\S/, @members;
		
		my @existingMembers = addGroupToLDAP($group,sidToID($r->{objectSid}));
		modifyGroup($group,\@existingMembers,\@members);
		runcmd("echo \"$group\t$r->{cn}\" >> $GROUPMAP");
	}	
}

sub cleanName
{
	my $name = shift @_;
	#ZR $name =~ s/\.//g;
	#$name =~ s/\W//g;
	$name =~ s/[^a-zA-Z0-9_. -]//g;
	$name=lc($name);
	push(@warnings, "\"$name\" was longer then unix standard, truncating.") if length($name) > 30;
	$name =~ s/.{30}\K.*//s;
	return $name;		
}

sub processUsers
{
	my @ldifs;
	my $i=0;
	my $results = getADEntries($ADUSERFILTER,$ADUSERATTRIBS);
	for my $r (@{$results})
	{
		logMSG($i++ .  ".) checking AD entry: " . $r->{sAMAccountName} . "\t" .$r->{mail}  ."\t". $r->{employeeID} . "\t" . $r->{employeeNumber} . "\t" . $r->{givenName} ."\t". $r->{sn}, 2);
		#set and format login
		my $login = cleanName($r->{sAMAccountName});
		
		
		my $employeeNumber = $r->{employeeNumber} || $r->{employeeID};	
		my $employeeID = $r->{employeeID} || $r->{employeeNumber};	
		my $oldDN = $r->{ADSRC};
		$ADCNToLogin{$oldDN} = $login;
		
		#get AD OUs for translation to unix groups
		my $oldDNclean = $oldDN;
		$oldDNclean =~ s/\s//g;
		while($oldDNclean =~ /ou=(\w+)/gi)
		{
			my $group = cleanName($1);
			push @{$groups{"ou_$group"}}, $login;
		}
	
		#use sid instead of the above (emplyeenumber etc)
		my $sid = sidToID($r->{objectSid});
		die "no sid for $r->{sAMAccountName}:$r->{objectSid} $sid" unless $sid =~ /^\d+$/;
	
		my $employeeNum = $employeeNumber || -1 ;
		push(@warnings, "NO employeeID AND NO employeeNumber for AD sAMAccountName=\"$r->{sAMAccountName}\" may skip depending on entry DN") if $employeeNum  < 1;
		push(@warnings, "NO GivenName/SN FOR found in AD for sAMAccountName=\"$r->{sAMAccountName}\" fn=\"$r->{givenName}\" sn=\"$r->{sn}\", (will use email address to determine first and last name)") unless $r->{givenName} && $r->{givenName};
		next unless $employeeNum > 0 || $oldDN =~ /OU=Research/ || $oldDN =~ /OU=IT,OU=Administrative/ ;
		push(@warnings, "Adding \"$r->{sAMAccountName}\" even though entry has no emplyeeID or employeeNumber (enry is in OU=Reseach or Administrative/IT") if $employeeNum  < 1;
	
		#use sid instead of uid
		my $uidNum = $sid;
	
		#test to make sure uid not used
		die "UID COLLISION: $uidNum already used for $ids{$uidNum}, cannot use $uidNum for $login\n" if $ids{$uidNum};
		$ids{$uidNum} = $r->{sAMAccountName};
	
		my $existsCheck = isUserInLdap($login);
		logMSG("user $login:$uidNum already is in ldap, skipping",2) if $existsCheck;
		next if $existsCheck;
		my ($altFn,$altSn) = split(/\./, $r->{sAMAccountName});
		my $fn = $r->{givenName} || $altFn; 
		my $sn = $r->{sn} || $altSn; 
		push(@warnings, "no givenName or sn for $r->{sAMAccountName}, SKIPPED (will not import)") if length($sn) < 2 || length($fn) < 2;
		next if length($sn) < 2;
		next if length($fn) < 2;
		my $ldif = getLDAPUserLDIF($login,$r->{sAMAccountName},$r->{mail},$uidNum,$fn,$sn,$oldDN);
		push @ldifs, $ldif;
		runcmd("echo \"$login\t$r->{sAMAccountName}\" >> $USERMAP") if $login ne $r->{sAMAccountName};
	}
	return join("\n",@ldifs);
}


sub getADEntries
{
	my $filter = shift @_;
	my $attribs = shift @_;
	my $rawAD = capturecmd("ldapsearch -LLL -o ldif-wrap=no -H $ADSERVER -x -D $ADBIND -w $ADBINDPASS -E pr=1000\/noprompt -b $ADBASE $filter $attribs");
	$rawAD =~ s/^dn: (.+\n)/dn: $1ADSRC: $1/gm;
	logMSG($rawAD,4);
	my @entries = split(/^dn: .+\n/m, $rawAD);
	my @results;
	for my $e (@entries)
	{
		next if length($e) < 10; 
		my @user = split("\n",$e);
		my %attrHash;
		for my $u (@user)
		{
			my ($key,$val) = split /\:+ /, $u;
			if($attribs =~ /$key/i && $key && $val && $key !~ /^#/ && $key =~ /\w+/)
			{
				$attrHash{$key} .= $attrHash{$key} ? "|" : "";
				$attrHash{$key} .= $val	
			}
		}
		push @results, \%attrHash;
	}
	return \@results;
}

sub insertLDIF
{
	my $ldif = shift @_;
	logMSG("Nothing new to add in ldap. done!",2) if length($ldif) < 10;
	return if length($ldif) < 10;
	my $ldifFile = File::Temp->new(SUFFIX => '.ldif');
	print $ldifFile $ldif;
	logMSG("adding the following to LDAP:\n$ldif",3);
	runcmd("ldapadd -x -D $LDAPBIND -w $LDAPPASS -f " . $ldifFile->filename);	
}
sub modifyGroup
{
	my $group = shift @_;
	my $oldMembersRef = shift @_;
	my @oldMembers = @$oldMembersRef;
	my $currentMembersRef = shift @_;
	my @currentMembers = @$currentMembersRef;

	my %removedMembers;
	@removedMembers{@oldMembers} = undef;
	delete @removedMembers{@currentMembers};
	push(@warnings, "removing $_ from $group") for keys %removedMembers;

	#print Dumper(@currentMembers);
	my $ldifFile = File::Temp->new(SUFFIX => '.ldif');
	my $ldif = "dn: cn=$group,$LDAPGROUPDN\n";
	$ldif .= "changetype: modify\n";#delete: memberUid\n" . $lines[1];
	$ldif .= "delete: memberUid\n" if %removedMembers;
	$ldif .= "memberUid: $_\n" for keys %removedMembers;
	$ldif .= "\n" if %removedMembers;
	$ldif .= "replace: memberUid\n";
	$ldif .= "memberUid: $_\n" for @currentMembers;
	print $ldifFile $ldif;
	logMSG("modifying the following to LDAP:\n$ldif",3);
	runcmd("ldapmodify -x -D $LDAPBIND -w $LDAPPASS -f " . $ldifFile->filename);	
}

#Get Last UID from LDAP
sub getLastLDAPuid
{
	my $ldapuidCMD = <<EC
ldapsearch -LLL -x -H $LDAPSERVER -D $LDAPBIND -w $LDAPPASS -b $LDAPBASE $LDAPFILTER | awk '/uidNumber/{a[\$2]=\$2}END{n=asort(a);print a\[n]+1}'
EC
;
	my $uidNum = capturecmd($ldapuidCMD);
	chomp $uidNum;
	return $uidNum;
}

#Get Last GID from LDAP
sub getLastLDAPgid
{
	my $ldapgidCMD = <<EC
ldapsearch -LLL -x -H $LDAPSERVER -D $LDAPBIND -w $LDAPPASS -b $LDAPBASE '(objectClass=posixGroup)' | awk '/gidNumber/{a[\$2]=\$2}END{n=asort(a);print a\[n]+1}'
EC
;
	my $gidNum = capturecmd($ldapgidCMD);
	chomp $gidNum;
	return $gidNum;
}
sub isUserInLdap
{
	my $login = shift @_;
	my $found = capturecmd("ldapsearch -LLL -x -H $LDAPSERVER -D $LDAPBIND -w $LDAPPASS -b $LDAPBASE '(&(objectClass=posixaccount)(uid=$login))' uid");
	chomp $found;
	return $found;
}
sub addGroupToLDAP
{
	my $group = shift @_;
	my $gid = shift @_ || getLastLDAPgid();
my $ldif = <<EOF
dn: cn=$group,ou=groups,dc=unix,dc=vai,dc=org
objectClass: posixGroup
objectClass: top
cn: $group
gidNumber: $gid
EOF
;
	insertLDIF($ldif);
	my $found = capturecmd("ldapsearch -LLL -x -H $LDAPSERVER -D $LDAPBIND -w $LDAPPASS -b $LDAPBASE '(&(objectClass=posixGroup)(cn=$group))' memberUid");
	my @members;
	while ($found =~ /memberUid:\s+(\S+)/g)
	{
		push @members, $1;
	}	
	return @members;
}
sub getLDAPUserLDIF
{
	my ($login,$sAMAccountName,$email,$uidNum,$fn,$sn,$oldDN) = @_;
my $ldif = <<EOF
dn: uid=$login,ou=people,dc=unix,dc=vai,dc=org
changetype: add
loginShell: /bin/bash
shadowMin: 8
sn: $sn
gidNumber: 1351800513
objectClass: inetOrgPerson
objectClass: posixAccount
objectClass: shadowAccount
mail: $email
uid: $login
shadowLastChange: 10877
shadowMax: 999999
uidNumber: $uidNum
shadowExpire: -1
shadowWarning: 7
cn: $fn $sn
givenName: $fn
gecos: $fn $sn
shadowFlag: 0
homeDirectory: /primary/home/$login
userPassword: {SASL}$sAMAccountName
description: $oldDN
EOF
;

	return $ldif;
}

sub capturecmd
{
        my $cmd = shift @_;
        logMSG("\`$cmd\`");
        return `$cmd`;
}

sub runcmd
{
        my $cmd = shift @_;
        logMSG("exec: $cmd");
        system($cmd);
}

sub logMSG
{
	my $msg = shift @_;
	my $level = shift @_ || 2;
	my $date = `date '+\%m/\%d/\%y \%H:\%M:\%S'`;
	chomp $date;
	$LOG = *STDOUT unless $LOG;
	if($level <= $DEBUG_LEVEL)
	{
		print $LOG "$date ";
		print $LOG (caller(1))[3] . "\t";
		my $level = 0;
		print $LOG "\t" while caller(++$level);
		print $LOG "$msg\n";	
	}
}

sub sidToID
{
	my $sid = shift @_;
	$sid = decode_base64($sid);
	my $lastSID;
	$lastSID =  "$_" for unpack "(H8)*",$sid;
	$lastSID = join '', reverse split /(..)/, $lastSID;
	$sid = oct("0x$lastSID");
	return $sid + $ADIDOFFSET;
}




