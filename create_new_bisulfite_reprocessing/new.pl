#!/usr/bin/perl


open(IN, $ARGV[0]);
@lines = <IN>;
chomp @lines;

for $entry (@lines)
{
	my $newDir;
	($flowcell,$path) = split(/\t+/, $entry);
	if($path =~ /^(.+)\/run(\d)$/)
	{
		$prefix = $1;
		$runN = $2;
		$runN ++;
		$newDir = "$prefix/run$runN\_20120701_novo" ;
	}
	elsif ($path =~ /^(.+)\/results\/\w+/ && $path !~ /run/)
	{
		$prefix = $1;
		$newDir = "$prefix/run1_20120701_novo" ;
	}
	
	next if !$newDir;
	
	print "$newDir\n";
	
	
	#mkdir $newDir;
	#ystem ("ln -s $newDir $flowcell");
	system ("cp $path/../../work*txt $newDir");

}
