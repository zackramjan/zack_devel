#!/usr/bin/perl
$pigz = "/home/uec-00/shared/production/software/pigz/pigz";
@files = glob("*_L001_R*.fastq.gz");

for my $f (@files)
{
	my $output = $f;
	$output =~  s/_L001_R/_L000_R/;
	$output =~  s/_S\d+_/_/;
	warn "$output already exists. SKIPPING. You can delete $output by hand and try again" if -e $output;
	next if -e $output;
	my $fileList;

	for my $i (1..4)
	{
		$f =~ s/_L00\d_R/_L00$i\_R/;
		$fileList .= "$f ";	
		die "$f expected to exist but not found\n" unless -e $f;


	}
	runcmd("$pigz -d -c $fileList | $pigz > $output");



}

sub runcmd{
	my $cmd=shift @_;
	my $caller=(caller(1))[3];
	print STDERR "$caller\t$cmd\n";
	system($cmd);
}


