#!/usr/bin/perl
use strict;

my $file = shift @ARGV || die;

#my $file = "$dupFile\.unmarkedDups.bam";

my $PICARD = "/home/uec-00/shared/production/software/picard/default/";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java -Xmx7G";
my $interval = "/export/uec-gs1/laird/shared/production/ga/external_analysis/ZackExome/ShibataVsGruber/S04380110_Regions.bed";
my $genome = "/home/uec-00/shared/production/genomes/encode_hg19_mf/male.hg19.fa";
$genome = "/home/uec-00/shared/production/genomes/hg19_rCRSchrm/hg19_rCRSchrm.fa" if $file =~ /rCRSchrm/;


my $subset = "5000000";

mkdir("$file\.work3");
chdir("$file\.work3");

#my $dupCmd = "$JAVA -Xmx4g -jar $PICARD/RevertSam.jar INPUT=$dupFile OUTPUT=$file SORT_ORDER=coordinate REMOVE_DUPLICATE_INFORMATION=true REMOVE_ALIGNMENT_INFORMATION=false RESTORE_ORIGINAL_QUALITIES=false ATTRIBUTE_TO_CLEAR=null CREATE_INDEX=true";

#my $BDcmd = "/home/uec-00/shared/production/software/uecgatk/default/uecgatk.pl  -T DownsampleDups -R /home/uec-00/shared/production/genomes/hg19_rCRSchrm/hg19_rCRSchrm.fa -I $file -o $file\.$subset\_trials100nt8.DownsampleDups.metric.txt -p $subset -trials 100 -nt 8";
my $DDcmd = "/home/uec-00/shared/production/software/uecgatk2/default/uecgatk.pl  -T BinDepthsExonWalker -R $genome -L $interval -I ../$file -o ../$file\.$subset\_winsize1.BinDepths.metric.wig -p $subset -winsize 1"; 


#runcmd($dupCmd);
#runcmd($BDcmd);
runcmd($DDcmd);




sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n";
        system($cmd);
}
