#!/usr/bin/perl
use strict;

my $file = shift @ARGV || die;

#my $file = "$dupFile\.unmarkedDups.bam";

my $PICARD = "/home/uec-00/shared/production/software/picard/default/";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java -Xmx7G";
my $interval = "/export/uec-gs1/laird/shared/production/ga/external_analysis/ZackExome/ShibataVsGruber/S04380110_Regions.bed";
my $genome = `/auto/uec-00/ramjan/devel/findGenome/findGenome.pl $file`;
chomp $genome;
exit unless $genome;

my $DDcmd = "/home/uec-00/shared/production/software/uecgatk2/default/uecgatk.pl -T DownsampleCoverageWalker -nt 4 -R $genome -I $file -o $file\.DownsampleCoverage.metric.txt"; 

runcmd($DDcmd);




sub runcmd
{
        my $cmd = shift @_;
        print STDERR "\n\n$cmd\n\n";
        system($cmd);
}
