#!/usr/bin/perl
use File::Basename;
$seqret = "/home/uec-00/shared/production/software/emboss/default/bin/seqret";
for my $file (@ARGV)
{
	die "file not found\n" unless -e $file;
	my $basefile = basename($file) . ".txt";
	print("$seqret fastq-sanger::$file fastq-illumina:$basefile");
	system("$seqret fastq-sanger::$file fastq-illumina:$basefile");
}
