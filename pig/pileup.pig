set pig.exec.mapPartAgg true;
set pig.exec.mapPartAgg.minReduction 5;
set pig.schematuple on;

reads = load 'out.bam' using BamLoader('yes'); 
 crossing_Reads = FILTER reads BY start/500!=end/500; 
 crossing_reads = FOREACH crossing_Reads GENERATE read, flags, refname, start, cigar, basequal, attributes#'MD', mapqual, name, end/500; 
 all_reads = FOREACH reads GENERATE read, flags, refname, start, cigar, basequal, attributes#'MD', mapqual, name, start/500; 
 input_reads = UNION crossing_reads, all_reads; 
 grouped_reads = GROUP input_reads BY (refname, $9); 
 dump grouped_reads;
pileup = FOREACH grouped_reads GENERATE BinReadPileup(input_reads,group.$1*500,(group.$1+1)*500); 
 pileup = FILTER pileup BY $0 is not null; 
 result = FOREACH pileup GENERATE flatten($0); 
 result = ORDER result BY chr, pos;
dump result;

