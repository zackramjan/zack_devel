#!/usr/bin/perl -w
use File::Temp qw/ tempfile tempdir /;

# - assumes single end data, see wrap_bwa_pe.pl for paired end alignment
# - assumes SA file will be named as read1.sai from read1.fq
my $USAGE = "wrap_bwa_se.pl refFa.fa";
die "$USAGE\n" unless (@ARGV >= 1);

my $bwa = "/auto/uec-00/shared/production/software/bwa-0.5.7/bwa";
my $refFa = $ARGV[0];
($fastqFH, $fastqFilename) = tempfile("fastqIn_XXXXX", SUFFIX => '.fastq');
($saiFH, $saiFilename) = tempfile("saiIn_XXXXX", SUFFIX => '.sai');


$start = 0;
while(<STDIN>)
{
  if($start == 0)
  {
    $start = 1 if  $_ =~ /^\@/;
  }
  print $fastqFH $_ if $start == 1;
}


# check existence of ref genome
die "reference does not exist.\n" if  ! -e $refFa;


my $cmd = join(" ", $bwa, "aln", "-t 2", $refFa, $fastqFilename, "> $saiFilename");
system($cmd);

my $alignCMD = join(" ", $bwa, "samse", $refFa, $saiFilename, $fastqFilename);
system($alignCMD);

unlink $fastqFilename;
unlink $saiFilename;
