-- use our in-house lib
register epiUDFs.jar;
register GenomeAnalysisTK.jar;
register sam-1.32.jar;




-----------------------------------------------------------------------
-- PERFORM ALIGMENT, FILTER READS
-----------------------------------------------------------------------

-- load the fastq input file using custom loader
fastqInput = load 's_2_sequence.txt' using edu.usc.epigenome.epiHadoop.FastQLoader as (fqID:chararray, fqSeq:chararray, fqIDD:chararray, fqQual:chararray);

-- Filter out contams
fastqInputNoContam = FILTER fastqInput by not edu.usc.epigenome.epiHadoop.IsContam(fqSeq);
--dump fastqInputNoContam;

--convert to sanger phred
fastqSanger = foreach fastqInputNoContam generate fqID, fqSeq, fqIDD, edu.usc.epigenome.epiHadoop.PhredToSanger(fqQual);

-- define a streaming processor that does bwa alignment
DEFINE alignbwa `/home/uec-00/ramjan/tmp/pig/wrap_bwa_se.pl /home/uec-01/shared/knowles/Genome/bwa/hg18_unmasked.fa ` input(stdin using PigStreaming('\n')) output(stdout using PigStreaming('\t'));
-- then run it on fastq input
sam = stream fastqSanger through alignbwa as (samQuery:chararray, samFlag:int, samRef:chararray, samPos:long, samMapq:int, samCigar:chararray, samMate:chararray, samMatePos:long, samInsertSize:int, samSeq:chararray, samQual:chararray);

-- filter out the unaligned reads
alignedSam = filter sam by samPos > 0;


--------------------------------------------------------------------------
-- Create PILEUP
--------------------------------------------------------------------------

-- break down each base BY position. EX: (chr2, 123213, 22, G, 38) = (refrence chrom, coordinate, cycle, base)
basePosNested = FOREACH alignedSam GENERATE samQuery, samFlag, samRef, edu.usc.epigenome.epiHadoop.SplitReadPos(samPos, samSeq, samQual), samMapq, samCigar, samMate, samMatePos, samInsertSize;
basePos = FOREACH basePosNested GENERATE samQuery, samFlag, samRef, FLATTEN($3), samMapq, samCigar, samMate, samMatePos, samInsertSize;
basePosSamOrder = FOREACH basePos GENERATE samQuery, samFlag, samRef, $3, samMapq, samCigar, samMate, samMatePos, samInsertSize, $5, $6, $4;
basePosPileup = GROUP basePosSamOrder by (samRef, $3) ;

--------------------------------------------------------------------------
-- Run Genotyper on pileup
--------------------------------------------------------------------------

genotyper = FOREACH basePosPileup GENERATE group.samRef, group.$1, edu.usc.epigenome.epiHadoop.SimpleGenotyper(*);
genotyper = filter genotyper by $2 is not null;
dump genotyper;



--------------------------------------------------------------------------
-- Create PILEUP WINDOW
--------------------------------------------------------------------------

-- pileupWindow = GROUP basePosPileup by (group.samRef, 10000, (int) (group.$1 / 10000)) ;
-- averageReadDepth = FOREACH pileupWindow GENERATE group.samRef, group.$1 * group.$2, edu.usc.epigenome.epiHadoop.AvgWindowCoverage(*); 
-- STORE averageReadDepth INTO 'avgwinresults';




