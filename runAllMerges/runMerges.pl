#!/usr/bin/perl
#
#
@dirs = glob("/export/uec-gs1/laird/shared/production/ga/merges/*");

@dirs = grep {-d $_} @dirs;
@dirs = grep {-e "$_/workFlowParams.txt" && !-e "$_/pbsrun.log.txt"} @dirs;

for my $dir (@dirs)
{
	chdir $dir;
	print "running merge in $dir\n";
	system("/home/uec-00/shared/production/software/ECWorkflow/submitWorkflow.pl workFlowParams.txt") unless $ARGV[0];

}
