#!/usr/bin/perl
use File::Basename;
my $NUMCORES = `cat /proc/cpuinfo | grep processor -c`;
chomp $NUMCORES;
$NUMCORES = $NUMCORES / 2;

my $RAM = 1.3 * $NUMCORES;
$RAM = int($RAM);
$RAM = 20 if $RAM > 20;

my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $PICARD = "/home/uec-00/shared/production/software/picard/default/";
my $UECGATK = "/home/uec-00/shared/production/software/uecgatk2/2014-07-18/uecgatk.pl";
my $JAVA = "/home/uec-00/shared/production/software/java/default_java7/bin/java -Xmx$RAM" . "G";
my $IGVTOOLS = "/home/uec-00/shared/production/software/igvtools/default/igvtools toTDF";

my $input = $ARGV[0] || die "need input bam file";
my $analysis = "DownsampleCoverageWalker";

$genome = `/auto/uec-00/ramjan/devel/findGenome/findGenome.pl $input`;
chomp $genome;
die "what genome is this? " unless $genome;
print STDERR "matched to genome: $genome\n";


runcmd("$UECGATK -T $analysis  -nt $NUMCORES -R $genome -I $input -o $input\.$analysis\.metric\.txt");


sub runcmd{
	my $cmd=shift @_;
	my $caller=(caller(1))[3];
	print STDERR "$caller\t$cmd\n";
	system($cmd);
}
