#!/usr/bin/perl
use Getopt::Long;
my $count;
GetOptions ('c=s' => \$count);
$count || die "how many lines to count of the end of the file";

for (@ARGV)
{
	my $line = $count =~ /\d+/ ? `tail -n $count $_` : `$count $_`;
	#print $line;
	my @lines = split(/\n/, $line);
	$list{$_} = \@lines;
}

print "Metric\t";
print "$_\t" for sort (keys %list);
print "\n";

foreach my $i (0..$count) 
{
	my $header ;
	for my $key (sort keys %list)
	{
		my $entry = $list{$key}[$i];
		my ($title,$val) = split(/\=/,$entry);
		if(!$header)
		{
			$header = $title;	
			print "$header\t";
		}
		print "$val\t";
	}
	print "\n";
}
