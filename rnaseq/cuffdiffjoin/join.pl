#!/usr/bin/perl

$refGtf = $ARGV[0] || die "no ref specified\nusage join.pl ref.gtf file.diff";
$diff = $ARGV[1] || die "no diff file specified\nusage join.pl ref.gtf file.diff";

open(IN, "<$refGtf");
while(<IN>)
{
	/gene_id \"(.+?)\"/;
	my $geneID = $1;
	/nearest_ref \"(.+?)\"/;
	my $ref = $1;

	$map{$geneID}{$ref} = 1;
}

for $gene (keys %map)
{
	for $ref (keys %{$map{$gene}})
	{
		$mapClean{$gene} .=  "$ref ";
	}
}
close IN;

open(IN, "<$diff");
while(<IN>)
{
	@line = split /\t/;
	$line[1] =  $mapClean{$line[0]};

	print join("\t", @line);
	
}
