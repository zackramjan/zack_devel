#!/usr/bin/perl

$input = @ARGV[0];
-e $input || die "cant find input file";

open(IN, "<$input");
print "Chromosome 	Start 	End 	Feature 	diff	pval\n";
while(<IN>)
{
	@row = split(/\t/);
#		print "$row[2]:$row[0]:$row[8]:$row[10]\n";
	if($row[2] =~ /^chr/)
	{
		$row[2] =~ s/[\:\-]/\t/g;
		$row[10] = neglog10($row[10]); 
		print "$row[2]\t$row[0]\t$row[8]\t$row[10]\n";
	}



}
sub neglog10 
{
	my $n = shift;
#	print "\t$n\n";
	return 20 if $n <= 0.00000;
	return log(1/$n)/log(10);
}
