#!/usr/bin/perl

$input = @ARGV[0];
-e $input || die "cant find input file";

open(IN, "<$input");
print "Chromosome 	Start 	End 	Feature	ln_foldchange	pval\n";
while(<IN>)
{
	@row = split(/\t/);
#		print "$row[2]:$row[0]:$row[8]:$row[10]\n";
	if($row[2] =~ /^chr/)
	{
		$row[2] =~ s/[\:\-]/\t/g;
		$row[8] =~ s/\d+\.\d+e\+308/1000/;
		print "$row[2]\t$row[0]\t$row[8]\t$row[10]\n";
	}



}
