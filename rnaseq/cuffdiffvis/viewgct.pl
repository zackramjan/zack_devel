#!/usr/bin/perl

$input = @ARGV[0];
-e $input || die "cant find input file";

open(IN, "<$input");
#print "Chromosome 	Start 	End 	Feature	fpkmX	fpkmY	pval\n";
while(<IN>)
{
	@row = split(/\t/);
#		print "$row[2]:$row[0]:$row[8]:$row[10]\n";
	if($row[2] =~ /^chr/)
	{
		#$row[2] =~ s/[\:\-]/\t/g;
		print "$row[0]\tna|\@$row[2]|\t$row[6]\t$row[7]\n";
	}



}
sub neglog10 
{
	my $n = shift;
#	print "\t$n\n";
	return 20 if $n <= 0.00000;
	return log(1/$n)/log(10);
}
