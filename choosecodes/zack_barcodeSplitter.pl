#!/usr/bin/perl
$maxSteps = 1000000;
$minDist = $ARGV[1];
$size = $ARGV[2];

open(IN,$ARGV[0]);
@codes = <IN>;
chomp @codes;
push @results, $codes[int(rand(@codes))];

while(scalar @results < $size)
{
	my $test;
	my $tmpCode = $codes[int(rand(@codes))];
	for my $code (@results)
	{
		$test = 1 if &hammingdist($tmpCode,$code) < $minDist;
		$maxSteps--;
	}
	push @results, $tmpCode unless $test == 1;
	die "Could not find solution\n" if $maxSteps < 0;
}

print "$_\n" for (@results);

sub hammingdist{ length( $_[ 0 ] ) - ( ( $_[ 0 ] ^ $_[ 1 ] ) =~ tr[\0][\0] ) }
