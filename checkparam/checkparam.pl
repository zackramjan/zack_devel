#!/usr/bin/env perl

$file = $ARGV[0] || die "specify a param file to check";
-e $file || die "param file does not exist";



open(IN, "<$file");

while($line = <IN>)
{
	$foundCS = 1 if $line =~ /ClusterSize\s*\=\s*\d+/;
	$foundFC = 1 if $line =~ /FlowCellName\s*\=\s*\S+/;
	$foundQ = 1 if $line =~ /queue\s*\=\s*\S+/;

	if($line =~ /^\s*(.+?)\s*\=\s*(.+)\s*$/)
	{
		my $param = $1;
		my $val = $2;
		print "$param is defined multiple times in file\n" if  $uniqParams{$param};
		$uniqParams{$param} ++;
		if($param =~ /Sample.+input/i)
		{
			my @files = split(",", $val);
			-e $_ || print "INPUT FILE \t $_ \tspecified in params file but not found!\n" for @files;
		}

		if($param =~ /Sample.+efer/i)
		{
			-e $val || -e "$val.fa" ||  print "REFERENCE GENOME \t $val  \t specified in param file but not found!\n";
		}

		#print "$param: $val\n";
	}
}

print "MISSING ClusterSize! (ex: ClusterSize = 1)\n" if(!$foundCS);
print "MISSING FlowCellName! (ex: FlowCellName = ZXC123XX)\n" if(!$foundFC);
print "MISSING queue! (ex: queue = laird)\n" if(!$foundQ);
