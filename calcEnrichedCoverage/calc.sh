#!/bin/bash
output=`basename $1`
output=${output}.histogram
echo /auto/uec-00/shared/production/software/bedtools/default/bin/coverageBed -hist -abam $1 -b $2      saved to $output  >&2
/auto/uec-00/shared/production/software/bedtools/default/bin/coverageBed -hist -abam $1 -b $2 | grep -v chr > $output
