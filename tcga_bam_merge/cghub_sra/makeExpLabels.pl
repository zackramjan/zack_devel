#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;
my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $bam = $ARGV[0] || die "specify a bam to submit";
-e $bam || die "bam not found";
-e "$bam\.md5" || die "bam.md5 not found";
$bam =~ /bam$/ || die "not a bam file";

my $bamMD5 = `cat $bam\.md5`;
$bamMD5 =~ s/\s+.+$//;
chomp $bamMD5;
$bam =~ /(TCGA.+)_(.{36})/;
$tcgaID = $1; $sampleUUID = $2;

my $readGroupsRaw = `samtools view -H $bam`;
my @readgroups = $readGroupsRaw =~ /\@RG\s+ID\:(\S+)/g;


	print <<EOF
<?xml version="1.0"?>
<EXPERIMENT_SET xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<EXPERIMENT center_name="USC-JHU"  alias="$tcgaID\_$sampleUUID">
  <TITLE>High Throughput bisulfite sequencing of $tcgaID</TITLE>
  <STUDY_REF refcenter="NHGRI" refname="phs000178" />
  <DESIGN>
    <DESIGN_DESCRIPTION>High Throughput bisulfite sequencing of $tcgaID</DESIGN_DESCRIPTION>
     <SAMPLE_DESCRIPTOR refcenter="TCGA" refname="$sampleUUID" />
    <LIBRARY_DESCRIPTOR>
      <LIBRARY_NAME>$tcgaID</LIBRARY_NAME>
      <LIBRARY_STRATEGY>Bisulfite-Seq</LIBRARY_STRATEGY>
      <LIBRARY_SOURCE>GENOMIC</LIBRARY_SOURCE>
      <LIBRARY_SELECTION>RANDOM</LIBRARY_SELECTION>
      <LIBRARY_LAYOUT>
        <SINGLE/>
      </LIBRARY_LAYOUT>
    </LIBRARY_DESCRIPTOR>
    <SPOT_DESCRIPTOR>
      <SPOT_DECODE_SPEC>
        <READ_SPEC>
          <READ_INDEX>0</READ_INDEX>
          <READ_LABEL>forward</READ_LABEL>
          <READ_CLASS>Application Read</READ_CLASS>
          <READ_TYPE>Forward</READ_TYPE>
          <BASE_COORD>1</BASE_COORD>
        </READ_SPEC>
      </SPOT_DECODE_SPEC>
    </SPOT_DESCRIPTOR>
  </DESIGN>
  <PLATFORM>
    <ILLUMINA>
      <INSTRUMENT_MODEL>Illumina HiSeq 2000</INSTRUMENT_MODEL>
    </ILLUMINA>
  </PLATFORM>
  <PROCESSING>
    <BASE_CALLS>
      <SEQUENCE_SPACE>Base Space</SEQUENCE_SPACE>
      <BASE_CALLER>Bustard (unknown version)</BASE_CALLER>
    </BASE_CALLS>
    <QUALITY_SCORES>
      <QUALITY_SCORER>Bustard (unknown version)</QUALITY_SCORER>
    </QUALITY_SCORES>
  </PROCESSING>
</EXPERIMENT>
</EXPERIMENT_SET>
EOF


