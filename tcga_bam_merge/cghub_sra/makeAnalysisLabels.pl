#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $bam = $ARGV[0] || die "specify a bam to submit";
-e $bam || die "bam not found";
-e "$bam\.md5" || die "bam.md5 not found";
$bam =~ /bam$/ || die "not a bam file";

my $bamMD5 = `cat $bam\.md5`;
$bamMD5 =~ s/\s+.+$//;
chomp $bamMD5;
$bam =~ /(TCGA.+)_(.{36})/;
$tcgaID = $1; $sampleUUID = $2;

my $readGroupsRaw = `samtools view -H $bam`;
my @readgroups = $readGroupsRaw =~ /\@RG\s+ID\:(\S+)/g;


print <<EOF
<?xml version="1.0"?>
<ANALYSIS_SET xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<ANALYSIS center_name="USC-JHU" alias="$tcgaID\_$sampleUUID\.analysis">
  <TITLE>High Throughput bisulfite sequencing of $tcgaID</TITLE>
  <STUDY_REF accession="SRP000677" refcenter="NHGRI" refname="phs000178" />
  <DESCRIPTION>High Throughput bisulfite sequencing of $tcgaID</DESCRIPTION>
  <ANALYSIS_TYPE>
    <REFERENCE_ALIGNMENT>
      <ASSEMBLY>
        <STANDARD short_name="HG19" />
      </ASSEMBLY>
      <RUN_LABELS>
EOF

;

foreach $rg (@readgroups)
{
	print <<EOF
       			 <RUN accession="" refcenter="USC-JHU" refname="$rg" read_group_label="$rg" data_block_name="$tcgaID\_$sampleUUID"/>
EOF
}

print <<EOF
      </RUN_LABELS>
      <SEQ_LABELS>
      	<SEQUENCE seq_label="chr1_gl000191_random" accession="gl000191" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr1_gl000192_random" accession="gl000192" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr4_gl000193_random" accession="gl000193" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr4_gl000194_random" accession="gl000194" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr7_gl000195_random" accession="gl000195" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr8_gl000196_random" accession="gl000196" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr8_gl000197_random" accession="gl000197" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr9_gl000198_random" accession="gl000198" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr9_gl000199_random" accession="gl000199" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr9_gl000200_random" accession="gl000200" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr9_gl000201_random" accession="gl000201" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr11_gl000202_random" accession="gl000202" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr17_gl000203_random" accession="gl000203" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr17_gl000204_random" accession="gl000204" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr17_gl000205_random" accession="gl000205" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr17_gl000206_random" accession="gl000206" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr18_gl000207_random" accession="gl000207" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr19_gl000208_random" accession="gl000208" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr19_gl000209_random" accession="gl000209" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr21_gl000210_random" accession="gl000210" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000211" accession="gl000211" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000212" accession="gl000212" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000213" accession="gl000213" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000214" accession="gl000214" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000215" accession="gl000215" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000216" accession="gl000216" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000217" accession="gl000217" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000218" accession="gl000218" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000219" accession="gl000219" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000220" accession="gl000220" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000221" accession="gl000221" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000222" accession="gl000222" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000223" accession="gl000223" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000224" accession="gl000224" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000225" accession="gl000225" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000226" accession="gl000226" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000227" accession="gl000227" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000228" accession="gl000228" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000229" accession="gl000229" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000230" accession="gl000230" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000231" accession="gl000231" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000232" accession="gl000232" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000233" accession="gl000233" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000234" accession="gl000234" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000235" accession="gl000235" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000236" accession="gl000236" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000237" accession="gl000237" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000238" accession="gl000238" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000239" accession="gl000239" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000240" accession="gl000240" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000241" accession="gl000241" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000242" accession="gl000242" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000243" accession="gl000243" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000244" accession="gl000244" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000245" accession="gl000245" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000246" accession="gl000246" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000247" accession="gl000247" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000248" accession="gl000248" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrUn_gl000249" accession="gl000249" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr1" accession="NC_000001.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr2" accession="NC_000002.11" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr3" accession="NC_000003.11" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr4" accession="NC_000004.11" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr5" accession="NC_000005.9" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr6" accession="NC_000006.11" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr7" accession="NC_000007.13" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr8" accession="NC_000008.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr9" accession="NC_000009.11" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr10" accession="NC_000010.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr11" accession="NC_000011.9" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr12" accession="NC_000012.11" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr13" accession="NC_000013.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr14" accession="NC_000014.8" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr15" accession="NC_000015.9" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr16" accession="NC_000016.9" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr17" accession="NC_000017.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr18" accession="NC_000018.9" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr19" accession="NC_000019.9" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr20" accession="NC_000020.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr21" accession="NC_000021.8" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chr22" accession="NC_000022.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrX" accession="NC_000023.10" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrY" accession="NC_000024.9" data_block_name="$tcgaID\_$sampleUUID"/>
		<SEQUENCE seq_label="chrM" accession="NC_012920.1" data_block_name="$tcgaID\_$sampleUUID"/>
      </SEQ_LABELS>
      <PROCESSING>
        <PIPELINE>
          <PIPE_SECTION section_name="bsmap">
            <STEP_INDEX>MAQ</STEP_INDEX>
            <PREV_STEP_INDEX>N/A</PREV_STEP_INDEX>
            <PROGRAM>bsmap</PROGRAM>
            <VERSION>2.01</VERSION>
          </PIPE_SECTION>
          <PIPE_SECTION section_name="MERGE">
            <STEP_INDEX>MERGE</STEP_INDEX>
            <PREV_STEP_INDEX>bsmap</PREV_STEP_INDEX>
            <PROGRAM>PICARD TOOLS</PROGRAM>
            <VERSION>1.46</VERSION>
            <NOTES>MergeSamFiles</NOTES>
          </PIPE_SECTION>
          <PIPE_SECTION section_name="DUPS">
            <STEP_INDEX>DUPS</STEP_INDEX>
            <PREV_STEP_INDEX>MERGE</PREV_STEP_INDEX>
            <PROGRAM>PICARD TOOLS</PROGRAM>
            <VERSION>1.46</VERSION>
            <NOTES>MarkDuplicates</NOTES>
          </PIPE_SECTION>            
        </PIPELINE>
        <DIRECTIVES>
          <alignment_includes_unaligned_reads>true</alignment_includes_unaligned_reads>
          <alignment_marks_duplicate_reads>true</alignment_marks_duplicate_reads>
          <alignment_includes_failed_reads>false</alignment_includes_failed_reads>
        </DIRECTIVES>
      </PROCESSING>
    </REFERENCE_ALIGNMENT>
  </ANALYSIS_TYPE>
  <TARGETS>
    <TARGET sra_object_type="SAMPLE" refcenter="TCGA" refname="$sampleUUID"/>
  </TARGETS>
  <DATA_BLOCK name="$tcgaID\_$sampleUUID">
    <FILES>
      <FILE checksum="$bamMD5" checksum_method="MD5" filetype="bam" filename="$bam" />
    </FILES>
  </DATA_BLOCK>
  <ANALYSIS_ATTRIBUTES>
    <ANALYSIS_ATTRIBUTE>
      <TAG>file format version</TAG>
      <VALUE>1.0</VALUE>
    </ANALYSIS_ATTRIBUTE>
    <ANALYSIS_ATTRIBUTE>
      <TAG>group order</TAG>
      <VALUE>none</VALUE>
    </ANALYSIS_ATTRIBUTE>
    <ANALYSIS_ATTRIBUTE>
      <TAG>sort order</TAG>
      <VALUE>coordinate</VALUE>
    </ANALYSIS_ATTRIBUTE>
  </ANALYSIS_ATTRIBUTES>
</ANALYSIS>
</ANALYSIS_SET>

EOF

