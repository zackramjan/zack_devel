#!/usr/bin/perl

$dir = $ARGV[0];
-d $dir || die "not a dir";
$dir =~ s/\///g;

$cmd = "curl -v -k -F \"analysisxml=\@$dir/analysis.xml\" -F \"runxml=\@$dir/run.xml\" -F \"experimentxml=\@$dir/experiment.xml\"  \"https://cghub.ucsc.edu/cghub/metadata/analysis/validate\"";

system($cmd);
