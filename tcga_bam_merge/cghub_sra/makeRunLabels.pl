#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;
my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $bam = $ARGV[0] || die "specify a bam to submit";
-e $bam || die "bam not found";
-e "$bam\.md5" || die "bam.md5 not found";
$bam =~ /bam$/ || die "not a bam file";

my $bamMD5 = `cat $bam\.md5`;
$bamMD5 =~ s/\s+.+$//;
chomp $bamMD5;
$bam =~ /(TCGA.+)_(.{36})/;
$tcgaID = $1; $sampleUUID = $2;

my $readGroupsRaw = `samtools view -H $bam`;

my @readgroups = $readGroupsRaw =~ /\@RG\s+ID\:(\S+)/g;

print <<EOF
<?xml version="1.0"?>
<RUN_SET xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
EOF
;


foreach $rg (@readgroups)
{  
	print <<EOF
<RUN center_name="USC-JHU" alias="$rg" accession="" >
 <EXPERIMENT_REF refcenter="USC-JHU" refname="$tcgaID\_$sampleUUID"/>
  <RUN_ATTRIBUTES>
    <RUN_ATTRIBUTE>
      <TAG>Illumina HiSeq 2000</TAG>
      <VALUE>yes</VALUE>
    </RUN_ATTRIBUTE>
   </RUN_ATTRIBUTES>
</RUN>
EOF
;
}

print <<EOF
</RUN_SET>
EOF
;
