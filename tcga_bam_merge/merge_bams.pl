#!/usr/bin/perl
use File::Spec;
use File::Basename;

my $SAMDIR = "/home/uec-00/shared/production/software/samtools";
my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";

my $output = shift @ARGV;

my @samples = glob("TCGA-A*");
for $sample (@samples)
{
	chdir $sample;
	my @libs = glob("*");
	for $lib (@libs)
	{
		chdir $lib;
		my @bams = glob("with_rg*.bam");
		my $output = "$sample\_$lib\.bam";
		#merge
		my $cmd = "VALIDATION_STRINGENCY=SILENT MERGE_SEQUENCE_DICTIONARIES=true CREATE_INDEX=true USE_THREADING=true MAX_RECORDS_IN_RAM=3000000 OUTPUT='$output' ";
		for my $bam (@bams)
		{
			$cmd .= "INPUT='$bam' ";
		}
		runcmd("$JAVA -Xmx12g -jar $PICARD/MergeSamFiles.jar $cmd");
		runcmd("$JAVA -Xms12g -Xmx12g -jar $PICARD/MarkDuplicates.jar CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT METRICS_FILE=dupmets.txt READ_NAME_REGEX=null INPUT=$output OUTPUT=$output\.mdups.bam"); 
		runcmd("mv $output.mdups* ..");
		
		chdir "..";
	}

	my @libams = glob("*.bam");
	my $output = "../$sample.bam";
	my $cmd = "VALIDATION_STRINGENCY=SILENT MERGE_SEQUENCE_DICTIONARIES=true CREATE_INDEX=true USE_THREADING=true MAX_RECORDS_IN_RAM=7000000 OUTPUT='$output' ";
	for my $bam (@libams)
	{
		$cmd .= "INPUT='$bam' ";
	}
	runcmd("$JAVA -Xmx22g -jar $PICARD/MergeSamFiles.jar $cmd");


	chdir "..";

}


sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n\n";
        system($cmd);
}
