#!/usr/bin/perl

$convFile = $ARGV[0] || die "file not found";

open(IN, $convFile);
while($line = <IN>)
{
	my @a = split("\t",$line);
	$sample{$a[0]} = $a[1];	
	#print "$a[0] = $a[1]\n";
}

@files = glob("TCGA*");
for my $f (@files)
{
	for my $s (keys %sample)
	{
		my $n = substr $s,0, 16;
		$n = substr $s,0, 20 if $s =~ /BRCA/;
		
		if($f =~ /$n/)
		{
			#print "$f\t$s\t$sample{$s}\t$n\n";
			$newName = $f;
			my $u = $sample{$s};
			$newName =~ s/$n/$s\_$u/;
			system("mv -i $f $newName\n");
			next;
		}
	}
}
