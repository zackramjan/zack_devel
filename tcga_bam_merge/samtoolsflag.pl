#!/usr/bin/perl

my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";

my $bam = $ARGV[0] || die "bam file needed";

my $output = "$bam\.flagstat.metric.txt";
die "bam not found" unless -e $bam;

system("$SAMTOOLS flagstat $bam > $output");
