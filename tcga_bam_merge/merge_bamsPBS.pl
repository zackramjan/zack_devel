#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Data::Dumper;
use strict;

my $SAMDIR = "/home/uec-00/shared/production/software/samtools";
my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";
my $i = 9999;


my @samples = glob("TCGA*");
print Dumper @samples;
for my $sample (@samples)
{
	chdir $sample;
	my @libs = glob("*");
	my @pbsIDs = ();;
	my @mduplibs = ();;

	for my $lib (@libs)
	{
		next unless -d $lib;
		chdir $lib;
		my @bams = glob("with_rg*.bam");
		my $output = "$sample\_$lib\.bam";
		#merge
		my $cmd = "VALIDATION_STRINGENCY=SILENT MERGE_SEQUENCE_DICTIONARIES=true CREATE_INDEX=true USE_THREADING=true MAX_RECORDS_IN_RAM=3000000 OUTPUT='$output' ";
		for my $bam (@bams)
		{
			$cmd .= "INPUT='$bam' ";
		}
		my $do = "$JAVA -Xmx12g -jar $PICARD/MergeSamFiles.jar $cmd\n";
		$do .= "$JAVA -Xms12g -Xmx12g -jar $PICARD/MarkDuplicates.jar CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT METRICS_FILE=dupmets.txt READ_NAME_REGEX=null INPUT=$output OUTPUT=$output\.mdups.bam\n"; 
		$do .= "mv $output.mdups* ..\n";
		push @pbsIDs,runPBS($do);
		push @mduplibs,"$output\.mdups.bam";
		chdir "..";
	}

	my $output = "$sample.bam";
	my $cmd = "VALIDATION_STRINGENCY=SILENT MERGE_SEQUENCE_DICTIONARIES=true CREATE_INDEX=true USE_THREADING=true MAX_RECORDS_IN_RAM=7000000 OUTPUT='$output' ";
	for my $bam (@mduplibs)
	{
		$cmd .= "INPUT='$bam' ";
	}
	my $domerge = "$JAVA -Xmx12g -jar $PICARD/MergeSamFiles.jar $cmd\n";
	$domerge .= "mv $sample.bam ..\n";
	$domerge .= "mv $sample.bai ..\n";
	my $waitString = join(":", @pbsIDs);
	print "wait1 $waitString\n";
	runPBS($domerge,$waitString);

	chdir "..";

}


sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n\n";
        system($cmd);
}

sub runPBS
{
	
	$i++;
        my $cmd = shift @_;
        my $waitString = shift @_;
	#print "wait2: $waitString\n";
        my $header .= "#PBS -l walltime=200:00:00\n";
        $header .= "#PBS -q lairdprio\n";
        $header .= "#PBS -l nodes=1:ppn=8:dx340\n";
	$header .= "#PBS -W depend=afterany:$waitString\n" if $waitString;
        $header .= "#PBS -d .\n";
        my $outfileName = "BamMerge." . $i . ".sh";
	print "$outfileName\n";
        open(OUT, ">$outfileName");
        print OUT $header;
        print OUT "$cmd\n";
        close OUT;
        my $ret = `qsub -k n $outfileName`;
        #my $ret = "$i\.hpc-pbs.usc.edu";
        $ret =~ /(\d\d\d\d+.hpc-pbs.usc.edu)/;
        $ret = $1;
	print "ret: $ret\n"	;
        return $ret;
}
