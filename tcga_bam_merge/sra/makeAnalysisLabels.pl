#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";


$dir = $ARGV[0] || die "specify a dir to process";
$library = $ARGV[1] || die "specify a dir to process";
$sample = $ARGV[2] || die "specify a dir to process";

-d $dir || die "dir not found";

@files = glob("$dir/*map.bam");

print <<EOF
<ANALYSIS center_name="USC_JHU" alias="$sample\.$library\.analysis" analysis_center="USC_JHU">
  <TITLE>High Throughput bisulfite sequencing of $sample\.$library</TITLE>
  <STUDY_REF accession="UNKNOWN" refcenter="UNKNOWN" refname="UNKNOWN" />
  <DESCRIPTION>High Throughput bisulfite sequencing of $sample\.$library</DESCRIPTION>
  <ANALYSIS_TYPE>
    <REFERENCE_ALIGNMENT>
      <ASSEMBLY>
        <STANDARD short_name="HG18" />
      </ASSEMBLY>
      <RUN_LABELS>
EOF

;

foreach $file (@files)
{
        my $fullpath = File::Spec->rel2abs( $file ) ;
        my $filebase = basename($file);
        my $date = `date`;
        chomp $date;

	$file =~ /(\w+)_(\w+)_s_(\d)\.map\.bam/;
	my $fc = $2;
	my $ln = $3;
	print <<EOF
        <RUN refcenter="USC_JHU" refname="$library\.$fc\.$ln" read_group_label="$fc\.$ln"  />
EOF

}

print <<EOF
      </RUN_LABELS>
      
      <PROCESSING>
        <PIPELINE>
          <PIPE_SECTION section_name="bsmap">
            <STEP_INDEX>MAQ</STEP_INDEX>
            <PREV_STEP_INDEX>N/A</PREV_STEP_INDEX>
            <PROGRAM>MAQ</PROGRAM>
            <VERSION>0.7.1</VERSION>
            <NOTES>maq map -M c -n 2</NOTES>
          </PIPE_SECTION>
          <PIPE_SECTION section_name="MERGE">
            <STEP_INDEX>MERGE</STEP_INDEX>
            <PREV_STEP_INDEX>MAQ</PREV_STEP_INDEX>
            <PROGRAM>PICARD TOOLS</PROGRAM>
            <VERSION>1.46</VERSION>
            <NOTES>MergeSamFiles</NOTES>
          </PIPE_SECTION>
          <PIPE_SECTION section_name="DUPS">
            <STEP_INDEX>DUPS</STEP_INDEX>
            <PREV_STEP_INDEX>MERGE</PREV_STEP_INDEX>
            <PROGRAM>PICARD TOOLS</PROGRAM>
            <VERSION>1.46</VERSION>
            <NOTES>MarkDuplicates</NOTES>
          </PIPE_SECTION>            
        </PIPELINE>
        <DIRECTIVES>
          <alignment_includes_unaligned_reads>false</alignment_includes_unaligned_reads>
          <alignment_marks_duplicate_reads>true</alignment_marks_duplicate_reads>
          <alignment_includes_failed_reads>false</alignment_includes_failed_reads>
        </DIRECTIVES>
      </PROCESSING>
    </REFERENCE_ALIGNMENT>
  </ANALYSIS_TYPE>
  <TARGETS>
    <TARGET accession="UNKNOWN" refcenter="UNKNOWN" refname="UNKNOWN" sra_object_type="SAMPLE" />
  </TARGETS>
  <DATA_BLOCK name="$sample\.$library">
    <FILES>
      <FILE checksum="MD5MD5MD5" checksum_method="MD5" filetype="bam" filename="filename.mdups.bam" />
    </FILES>
  </DATA_BLOCK>
  <ANALYSIS_ATTRIBUTES>
    <ANALYSIS_ATTRIBUTE>
      <TAG>file format version</TAG>
      <VALUE>1.0</VALUE>
    </ANALYSIS_ATTRIBUTE>
    <ANALYSIS_ATTRIBUTE>
      <TAG>group order</TAG>
      <VALUE>none</VALUE>
    </ANALYSIS_ATTRIBUTE>
    <ANALYSIS_ATTRIBUTE>
      <TAG>sort order</TAG>
      <VALUE>coordinate</VALUE>
    </ANALYSIS_ATTRIBUTE>
  </ANALYSIS_ATTRIBUTES>
</ANALYSIS>

EOF

