#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";



@files = glob("TCGA*/*/*.bam");

foreach $file (@files)
{
	next if $file =~ /with_rg/;
        my $fullpath = File::Spec->rel2abs( $file ) ;
        my $filebase = basename($file);
	my $filedir = dirname($file);
        my $date = `date`;
	my $library = basename(dirname($file));
	my $sample = basename(dirname(dirname($file)));
        chomp $date;

	
	$filebase =~ /^(\w+)_(\w+)_(\d+).+\.bam$/;
	my $fc = $2;
	my $ln = $3;
	push @cmds, "$JAVA -Xmx12g -jar $PICARD/AddOrReplaceReadGroups.jar VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate MAX_RECORDS_IN_RAM=3000000 INPUT='$file' OUTPUT='$filedir/with_rg_$filebase' RGID='$fc\.$ln' RGLB='$library' RGPL='illumina Hiseq' RGPU='$fc\.$ln' RGSM='$sample' RGCN='USC EPIGENOME CENTER' RGDS='from file $fullpath on $date'\n" unless -e "$filedir/with_rg_$filebase";

}

#create pbs files
foreach $i (0..$#cmds)
{
	my $header = "#PBS -q laird\n";
	$header .= "#PBS -l walltime=200:00:00\n";
	$header .= "#PBS -l nodes=1:ppn=8:dx340\n";
	$header .= "cd $pwd\n";
	my $outfileName = "fixBam.$i.sh";
	open(OUT, ">$outfileName");
	print OUT $header;
	print OUT $cmds[$i];
	close OUT;
	system("qsub $outfileName");

}


