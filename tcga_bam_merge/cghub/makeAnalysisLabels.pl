#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $bam = $ARGV[0] || die "specify a bam to submit";
-e $bam || die "bam not found";
-e "$bam\.md5" || die "bam.md5 not found";
$bam =~ /bam$/ || die "not a bam file";

my $bamMD5 = `cat $bam\.md5`;
$bamMD5 =~ s/\s+.+$//;
chomp $bamMD5;
my ($tcgaID, $sampleUUID) = split("_", $bam);

my $readGroupsRaw = `samtools view -H $bam`;

my @readgroups = $readGroupsRaw =~ /\@RG\s+ID\:(\S+)/g;

print <<EOF
<?xml version="1.0"?>
<ANALYSIS_SET xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<ANALYSIS center_name="USC-JHU" alias="$tcgaID\_$sampleUUID\.analysis">
	 <TITLE>High Throughput bisulfite sequencing of $tcgaID</TITLE>
	  <STUDY_REF refcenter="NHGRI" refname="phs000178" />
	  <DESCRIPTION>High Throughput bisulfite sequencing of $tcgaID</DESCRIPTION>
	  <TARGETS>
	  	<TARGET sra_object_type="SAMPLE" refcenter="TCGA" refname="$sampleUUID"/>
	  </TARGETS>
	  <DATA_BLOCK> 
		<FILES>
			  <FILE checksum="$bamMD5" checksum_method="MD5" filetype="bam" filename="$bam"/>
		 </FILES> 
	 </DATA_BLOCK>
	
	  <ANALYSIS_TYPE>
		<REFERENCE_ALIGNMENT>
		  <ASSEMBLY>
			<STANDARD short_name="HG19" />
		  </ASSEMBLY>
		  <RUN_LABELS>
EOF

;

foreach $rg (@readgroups)
{
	print <<EOF
       			 <RUN refcenter="USC-JHU" refname="$rg" read_group_label="$rg"  />
EOF
}

print <<EOF
		  </RUN_LABELS>
		</REFERENCE_ALIGNMENT>
	  </ANALYSIS_TYPE>
	</ANALYSIS>
</ANALYSIS_SET>
EOF

