#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;
my $pwd = getcwd;

my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $bam = $ARGV[0] || die "specify a bam to submit";
-e $bam || die "bam not found";
-e "$bam\.md5" || die "bam.md5 not found";
$bam =~ /bam$/ || die "not a bam file";

my $bamMD5 = `cat $bam\.md5`;
$bamMD5 =~ s/\s+.+$//;
chomp $bamMD5;
my ($tcgaID, $sampleUUID) = split("_", $bam);

my $readGroupsRaw = `samtools view -H $bam`;
my @readgroups = $readGroupsRaw =~ /\@RG\s+ID\:(\S+)/g;

	print <<EOF
<?xml version="1.0"?>
<EXPERIMENT_SET xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<EXPERIMENT center_name="USC-JHU"  alias="$tcgaID\_$sampleUUID">
  <STUDY_REF refcenter="NHGRI" refname="phs000178" />
  <DESIGN>
    <SAMPLE_DESCRIPTOR refcenter="TCGA" refname="$sampleUUID" />
    <LIBRARY_DESCRIPTOR>
      <LIBRARY_NAME>$tcgaID</LIBRARY_NAME>
      <LIBRARY_STRATEGY>Bisulfite-Seq</LIBRARY_STRATEGY>
      <LIBRARY_SOURCE>GENOMIC</LIBRARY_SOURCE>
      <LIBRARY_SELECTION>RANDOM</LIBRARY_SELECTION>
    </LIBRARY_DESCRIPTOR>
  </DESIGN>
  <PLATFORM>
    <ILLUMINA>
      <INSTRUMENT_MODEL>Illumina HiSeq 2000</INSTRUMENT_MODEL>
      <CYCLE_COUNT>210</CYCLE_COUNT>
      <SEQUENCE_LENGTH>210</SEQUENCE_LENGTH>
    </ILLUMINA>
  </PLATFORM>
 </EXPERIMENT>
</EXPERIMENT_SET>
EOF


