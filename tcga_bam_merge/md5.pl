#!/usr/bin/perl

my $bam = $ARGV[0] || die "bam file needed";

my $output = "$bam\.md5";
die "bam not found" unless -e $bam;

system("md5sum $bam > $output");
