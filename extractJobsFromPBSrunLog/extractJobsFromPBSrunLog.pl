#!/usr/bin/perl

$job = $ARGV[0] || die;
$input = $ARGV[1] || die;
-e $input || die;

open(IN, $input);
while (<IN>) { $log .= $_ }

@jobs = split(/############/m,$log);


for(@jobs)
{
	$_ =~ s/#PBS \-W depend=.+\n//;
	push @matchedJobs, $_ if $_ =~ /$job/m;

}

$job =~ s/\W/_/g;

print STDERR "writing extracted jobs to dir: \"extractedJob_$job\"\n";
mkdir "extractedJob_$job";
chdir "extractedJob_$job";
for(@matchedJobs)
{
	$outputFileName = ++$i . ".$job\.pbs";
	open (OUT, ">$outputFileName") || die;
	print OUT $_;
	print STDERR "wrote job to extractedJob_$job/$outputFileName\n";
	close OUT;
}

