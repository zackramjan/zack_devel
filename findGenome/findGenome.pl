#!/usr/bin/perl
use Digest::MD5 qw(md5 md5_hex md5_base64);

my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my @genomeFiles = glob "/home/uec-00/shared/production/genomes/*/*.dict";
my $bam = $ARGV[0] || die;
-e $bam || die "file not found\n";

my $bamMD5 = calcRefChecksum($bam);


for (@genomeFiles)
{
	if ($bamMD5 eq calcRefChecksum($_))
	{
		my $result = $_;
		$result =~ s/dict$/fa/;
		print "$result\n" if -e $result;
	}
}




sub calcRefChecksum
{	
	my $file = shift @_;
	my $input;
	$input = `cat $file` if $file =~ /dict$/;
	$input = `$SAMTOOLS view -H $file` if $file =~ /bam$/;
	return  unless $input;

	my @contigs;
	while($input =~ /^\@SQ\s+SN:(\S+)\s+LN:(\S+)/mg)
	{
		push @contigs, "$1 $2";
	}

	@contigs = sort @contigs;
	return unless @contigs;
	my $contigStr = join "-", @contigs;
	

	#print STDERR "CALC $file : " . md5_hex($contigStr)  . "\n";
	return md5_hex($contigStr);

}






