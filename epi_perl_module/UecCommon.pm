package UecCommon;
use strict;
use Exporter;
use File::Temp qw/ tempfile tempdir /;

#######################
# ZACK RAMJAN 2012
# I wanted to group common convenience funcs into a single module
######################
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);



$VERSION     = 0.10;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = qw(&runcmd &getNCores $SAMTOOLS $PICARD $JAVA &addReadGroup &markDups &runPBS);
%EXPORT_TAGS = ( DEFAULT => [qw(&runcmd &getNCores $SAMTOOLS $PICARD $JAVA &addReadGroup &markDups &runPBS)]);
my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $PICARD = "/home/uec-00/shared/production/software/picard/default/";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";

sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n";
        system($cmd);
}

sub getNCores
{
	my $numcores = `cat /proc/cpuinfo | grep processor -c`;
	chomp $numcores;
	return $numcores;
}

sub addReadGroup
{
	my $date = `date`; chomp $date;
	my $output = shift @_ || die;
	-s $output || die;
	my $flowcell = shift @_ || die "flowcell";
	my $lane = shift @_ || die "lane";
	my $lib = shift@_ || die "lib";
	my $sample = shift @_ || $lib;
	runcmd("$JAVA -Xmx4g -jar $PICARD/AddOrReplaceReadGroups.jar CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate MAX_RECORDS_IN_RAM=1000000 INPUT='$output' OUTPUT='with_rg_$output' RGID='$flowcell\.$lane' RGLB='$lib' RGPL='illumina Hiseq' RGPU='$flowcell\.$lane' RGSM='$sample' RGCN='USC EPIGENOME CENTER' RGDS='from file $output on $date'");
	#overwrite non-readgroups bams
	runcmd("mv with_rg_$output $output");
	my $bai = "with_rg_$output";
	$bai =~ s/bam$/bai/;
	runcmd("mv $bai $output.bai");

}

sub markDups
{
	my $output = shift @_ || die "file";
	-s $output || die "file";
	my $outputdups = $output;
	$outputdups =~ s/bam$/mdups\.bam/;
	runcmd("$JAVA -Xms12g -Xmx12g -jar $PICARD/MarkDuplicates.jar CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT METRICS_FILE=dupmets.txt READ_NAME_REGEX=null INPUT=$output OUTPUT=$outputdups");
	my $dupbai = $outputdups;
	$dupbai =~ s/bam$/bai/;
	runcmd("mv $dupbai $outputdups\.bai");
}

sub runPBS
{
	
        my $cmd = shift @_;
        my $waitString = shift @_;
        my $header .= "#PBS -l walltime=200:00:00\n";
        $header .= "#PBS -q lairdprio\n";
        $header .= "#PBS -l nodes=1:ppn=8:dx340\n";
	$header .= "#PBS -W depend=afterany:$waitString\n" if $waitString;
        $header .= "#PBS -d .\n";
	(my $fh, my $outfilename) = tempfile("UEC_PBS_XXXXXXXX", SUFFIX => ".sh");
        print $fh $header;
        print $fh "$cmd\n";
	close $fh;
        my $ret = `qsub -k n $outfileName`;
        #my $ret = "$i\.hpc-pbs.usc.edu";
        $ret =~ /(\d\d\d\d+.hpc-pbs.usc.edu)/;
        $ret = $1;
	print STDERR "ret: $ret\n"	;
	unlink $outfilename;
        return $ret;
}




1; #loaded ok
