#!/usr/bin/perl
#CONSTANTS
$illsrf = "/auto/uec-00/shared/production/software/GAPipeline-1.6.0a9/bin/illumina2srf"; 

#ARGS
$qseqDir = $ARGV[0] || die; #DIR WITH QSEQS
$outputDir = $ARGV[1] || die; #THE DIR TO SAVE SRFS IN
$qseqDir .= "/";
$qseqDir = "" if "$qseqDir" =~ /^\.\/+/;

for $laneNum (2..8)
{
	my $inputs = "";
	my $output = "$outputDir/s_$laneNum" . "_traces.srf";
	die "$output already exists" if -e $output;

	for my $i (1..120)
	{
		my $tile = sprintf("%0*d",4,$i);
		my $qseq = "$qseqDir" . "s_$laneNum" . "_1_$tile". "_qseq.txt";
		-e $qseq || die "could not find $qseq";
		$inputs .= "$qseq ";
	}

	my $cmd = "$illsrf -P -o $output $inputs";
	print "$cmd \n";
	system($cmd);
}
