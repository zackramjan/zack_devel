#!/usr/bin/perl
use Getopt::Long;
$VCFSORT = "/home/uec-00/shared/production/software/VCFTools/default/bin/vcf-sort";
$usage =  "merge.pl -i a.vcf -i b.vcf -o output.vcf\n";

 GetOptions ("o=s" => \$output, "i=s" => \@files  )   # flag
  or die($usage);

@files && $output || die $usage;

unlink $output if -e $output;

for my $i (0..$#files)
{
	print STDERR "$files[$i] > $output\n";
	open IN, "<$files[$i]";
	open OUT, ">>$output";
	
	while (<IN>)
	{
			print OUT $_ unless substr($_, 0, 1) eq "#" && $i > 0;
	}
	close IN; 
	close OUT;
} 

print STDERR "sorting with vcf-sort\n";
system("$VCFSORT $output > $output\.tmp");
print STDERR "removing duplicate lines\n";
system("uniq $output\.tmp > $output");
unlink "$output\.tmp";
print STDERR "Done\n";



























