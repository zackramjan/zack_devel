#!/usr/bin/perl
use autodie;
my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
-e ($input = $ARGV[0]) || die "input not found\n";
$lib = $ARGV[1] || die "new lib name req\n";

$header = `$SAMTOOLS view -H $input`;

$header =~ s/SM:\S+/SM:$lib/mg;
$header =~ s/LB:\S+/LB:$lib/mg;

open($fh,">","$input\.HEADER");
print $fh $header;
close($fh);
runcmd("$SAMTOOLS reheader $input\.HEADER $input > $input.NEWHEADER");
runcmd("mv -i $input $input\.OLDHEADER");
runcmd("mv -i $input\.NEWHEADER $input");
runcmd("$SAMTOOLS index $input");

sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n";
        system($cmd);
}

