#!/usr/bin/perl
use File::Basename;
use File::Spec;

$SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $bwa = "/home/uec-00/shared/production/software/bwa/latest/bwa";

my $fullname = @ARGV[0];
($name,$path) = fileparse(File::Spec->rel2abs($fullname));
die if (glob("$path/*salmo*"));

print STDERR "changing dir to $path\n";
chdir($path);

my $contamName = `ls -S ContamCheck.tair8* | head -n 1`;
chomp $contamName;
-e $contamName || die "could not find existing contam files in $path";

my $bam = `ls -S *mdups.bam | head -n 1`;
chomp $bam;
-e $bam || die "could not find bam in $path";

runcmd("$SAMTOOLS view -h -f 4 $bam  | head -n 5000050 > $bam\.unaln.sam"); 
runcmd("$SAMTOOLS view -b -S $bam\.unaln.sam > $bam\.unaln.bam"); 
runcmd("$bwa aln -t 7 -b0 /auto/uec-00/shared/production/genomes/salmon/salmosalar.fa $bam\.unaln.bam > $bam\.unaln.sai");
runcmd("$bwa samse /auto/uec-00/shared/production/genomes/salmon/salmosalar.fa $bam\.unaln.sai $bam\.unaln.bam > $bam\.salmo.sam");
runcmd("$SAMTOOLS view -S -b -h $bam\.salmo.sam > $bam\.salmo.bam");
$contamName =~ s/tair8.pluscontam.fa/salmosalar.fa/;
runcmd("$SAMTOOLS flagstat $bam\.salmo.bam > $contamName");
runcmd("rm $bam\.unaln.sam $bam\.unaln.bam $bam\.unaln.sai $bam\.salmo.sam $bam\.salmo.bam");


sub runcmd
{
        my $cmd = shift @_;
	print STDERR "$cmd\n";
	system($cmd);
}

