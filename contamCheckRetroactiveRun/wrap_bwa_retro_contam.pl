#!/usr/bin/perl -w
# Mon Jul 12 14:03:06 PDT 2010

my $usage = "wrap_bwa.pl refFa.fa read1.fq [read2.fq] [outfile name]";
die "$usage\n" unless (@ARGV >= 3);

my $bwa = "/home/uec-00/shared/production/software/bwa/default/bwa";
my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";

$refFa = $ARGV[0];
$read1 = $ARGV[1];
my $read1SA = $read1;
$read1SA =~ s/\.fq$//i;
$read1SA .= ".sai";
$outfile = $ARGV[$#ARGV];

die "$outfile already exists" if (-e $outfile);

$outfileSAM = $outfile . ".sam";

# check existence of ref genome
die "reference does not exist.\n" if (! -e $refFa);

# check existence of read sequence files
die "need read sequence files\n" unless ( -e $read1 );

#check phred of read
my $phred = `/home/uec-00/shared/production/software/perl_utils_usc/testFastqQualityScale.pl $read1`;
$phred = $phred =~ /64/ ? "-I" : "";

#aln end 1
my $cmd = join(" ", $bwa, "aln", $phred, "-t 15", $refFa, $read1, "> $read1SA");
runcmd($cmd);

die "need read.sai files\n" unless ( -e $read1SA );


if (@ARGV == 4)
{
	$read2 = $ARGV[2];

	# check existence of read sequence files
	die "need read sequence files\n" unless ( -e $read2 );

	$read2SA = $read2;
	$read2SA =~ s/\.fq$//i;
	$read2SA .= ".sai";

	$cmd = join(" ", $bwa, "aln", $phred, "-t 15", $refFa, $read2, "> $read2SA");
	runcmd($cmd);

	die "need read.sai files\n" unless ( -e $read1SA && -e $read2SA );
	my $alignCMD = join(" ", $bwa, "sampe", $refFa, $read1SA, $read2SA, $read1, $read2, "> $outfileSAM");
	runcmd($alignCMD);
}
else
{
	my $alignCMD = join(" ", $bwa, "samse", $refFa, $read1SA, $read1, "> $outfileSAM");
	runcmd($alignCMD);

}


runcmd("samtools view -S -b -h -o $outfile $outfileSAM");
#runcmd("$JAVA -Xmx4g -jar $PICARD/SortSam.jar VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate INPUT=$outfileSAM OUTPUT=$outfile\.sorted.bam");
#runcmd("$JAVA -Xmx4g -jar $PICARD/ReorderSam.jar VALIDATION_STRINGENCY=SILENT REFERENCE=$refFa INPUT=$outfile\.sorted.bam OUTPUT=$outfile");

sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n";
        system($cmd);
}
