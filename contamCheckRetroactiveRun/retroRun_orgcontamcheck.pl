#!/usr/bin/perl 
use File::Basename;
use Cwd;
 use Cwd 'abs_path';
# Mon Jul 12 14:03:06 PDT 2010


my $bwa = "/export/uec-gs1/laird/shared/research/zack/tmp/rerunContam/wrap_bwa.pl";
my $SAMTOOLS = "/home/uec-00/shared/production/software/samtools/samtools";
my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";

@genomes = qw{/home/uec-00/shared/production/genomes/encode_hg19_mf/female.hg19.fa  /home/uec-00/shared/production/genomes/sacCer1/sacCer1.fa  /home/uec-00/shared/production/genomes/phi-X174/phi_plus_SNPs.fa  /home/uec-00/shared/production/genomes/arabidopsis/tair8.pluscontam.fa  /home/uec-00/shared/production/genomes/mm9_unmasked/mm9_unmasked.fa  /home/uec-00/shared/production/genomes/Ecoli/EcoliIHE3034.fa  /home/uec-00/shared/production/genomes/rn4_unmasked/rn4.fa  /home/uec-00/shared/production/genomes/salmon/salmosalar.fa  /home/uec-00/shared/production/genomes/rRNA/rRNA.fa  /home/uec-00/shared/production/genomes/lambdaphage/NC_001416.fa};


my $sampleSize = 5000000;
$subsample = $sampleSize * 4;

-e $ARGV[0] || die "input list not specified\n";
open(IN,"<$ARGV[0]");
my @lines = <IN>;

for my $line (@lines)
{
	#get fastq
	my ($mdups,$input) = split(/\s+/, $line);
	print STDERR "processing $input\n";
	$input = getPath($input);
	next unless -e $input;
	my $inputBase = basename($input);

	#chdir to results
	chdir(dirname($mdups)) || next;
	print STDERR "changing to dir " .  getcwd . "\n";
	mkdir("contamTmp");
	chdir("contamTmp");

	#get subsample

	for my $genome ( @genomes )
	{
		
		my $genomeBase = basename($genome);
		my @contamFile = glob("../ContamCheck\.$genomeBase\.*");
		print STDERR $contamFile[0] . ".flagstat.txt already exists, skipping\n" if @contamFile;
		next if @contamFile;

		if(! -e "$inputBase\.sampled.txt")
		{
			runcmd("head -n $subsample $input > $inputBase\.sampled.txt") if ($input !~ /gz$/ && $input !~ /bz2$/);
			runcmd("zcat $input | head -n $subsample > $inputBase\.sampled.txt") if($input =~ /gz$/);
			runcmd("bzcat $input | head -n $subsample > $inputBase\.sampled.txt") if($input =~ /bz2$/);
		}
		runcmd("$bwa $genome $inputBase\.sampled.txt $inputBase\.$genomeBase\.bam");
		runcmd("$SAMTOOLS flagstat  $inputBase\.$genomeBase\.bam > ContamCheck\.$genomeBase\.$sampleSize\.$inputBase\.flagstat.txt");
		runcmd("mv ContamCheck\.$genomeBase\.$sampleSize\.$inputBase\.flagstat.txt ..");
	}
	
	chdir("..");
	runcmd("rm -r contamTmp");
}



sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n";
        system($cmd);
}



sub getPath
{
	my $file = shift @_;
	print STDERR "RESOLVED $file -> ";
	$file = abs_path($file);
	$file = abs_path(readlink $file) if -l $file;
	$file = "$file\.zip"  if !-e $file && -e "$file\.zip";
	$file = "$file\.gz"  if !-e $file && -e "$file\.gz";
	$file = "$file\.bz2"  if !-e $file && -e "$file\.bz2";
	print STDERR  "$file\n";
	return $file if -e $file;
	print STDERR "$file was not found, broken symlink?\n";
}


