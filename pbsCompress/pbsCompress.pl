#!/usr/bin/perl
use File::Spec;

$cpus = pop @ARGV || die "specify number of cpus to use";
$cpus =~ /^\d+$/ || die "cpus must be a number";

-d $ARGV[$_] || die "$_ not found, specify a dir to compress" for (@ARGV);

$dir = join(" ", @ARGV);

my $findCmd = "find $dir -type f -size +3M\n";
print "$findCmd\n";

@files = split(/\n/,`$findCmd`);
@files = grep(!/htm.*$/i,@files);
@files = grep(!/png$/i,@files);
@files = grep(!/gz$/i,@files);
@files = grep(!/bgzf$/i,@files);
@files = grep(!/bcl$/i,@files);
@files = grep(!/bw$/i,@files);
@files = grep(!/tdf$/i,@files);
@files = grep(!/zip$/i,@files);
@files = grep(!/jpg$/i,@files);
@files = grep(!/bzip2$/i,@files);
@files = grep(!/bz.*$/i,@files);
@files = grep(!/ba.*$/i,@files);
@files = grep(!/xml$/i,@files);

$tmpBase = $dir;
if($tmpBase =~ /\//)
{
	$tmpBase =~ /^.+\/(.+?)$/;
	$tmpBase = $1;
}

#split data into many pbs jobs
foreach $i  (0..$#files)
{
	$cmd[$i % $cpus] .= "pbzip2 " . File::Spec->rel2abs($files[$i]) ."\n";
}


#create pbs files
foreach $i (0..$#cmd)
{
	my $header = "#PBS -q laird\n";
	$header .= "#PBS -l walltime=100:00:00\n";
	$header .= "#PBS -l nodes=1:ppn=8:quadcore\n";
	my $outfileName = "pbsCompress" . $tmpBase . ".$i.sh";
	mkdir("pbsCompress_" . $tmpBase);
	chdir("pbsCompress_" . $tmpBase);
	open(OUT, ">$outfileName");
	print OUT $header;
	print OUT $cmd[$i];
	close OUT;
	system("qsub $outfileName");
	chdir("..");

}


