#!/usr/bin/perl
use File::Basename;




@files = @ARGV;


@files = sort @files;
for my $f (@files)
{
	next unless $f =~ /fastq/;
	$fb = basename($f);
	$fb =~ /(.+?_R\d)_(\d\d\d)\.fastq.gz/;
	my $prefix = $1;
	my $number = $2;
	runcmd("zcat $f > $prefix\.fastq") if $prefix eq "001";
	runcmd("zcat $f >> $prefix\.fastq") unless $prefix eq "001";




}

sub runcmd{
	my $cmd=shift @_;
	my $caller=(caller(1))[3];
	print STDERR "$caller\t$cmd\n";
	system($cmd);
}
