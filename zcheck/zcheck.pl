#!/usr/bin/perl
use IO::Socket;
#Zack's system checker. quick and dirty
$recurseDepth = 3; #on error, try 3 times 
$sshCmd = "ssh -x -f -n";
$outputFile = $ARGV[0] || die "need output file";

do
{
	undef $hosts;
	&checkHosts();
	$errorList = &analyze();
	$recurseDepth--;
} while ($errorList && $recurseDepth > 0);


&doFail($errorList);

open(OUTPUTFILE, "> $outputFile");
print OUTPUTFILE &resultsHTMLinline();
close OUTPUTFILE;

#&resultsText;




sub checkHosts
{
	open(HOSTS, "< " . $ARGV[1]) || die;
	while(<HOSTS>)
	{
		if(!($_ =~ /^#/))
		{
			@toCheck = split " ", $_;
			$host = shift @toCheck;
			for $i (@toCheck)
			{
				if ($i =~ /(alive)/) { $hosts{$host}{$1} = &checkAlive($host); $allChecks{"alive"} = 1;}
				elsif ($i =~ /(diskspace)/) { $hosts{$host}{$1} = &checkDiskspace($host); $allChecks{"diskspace"} = 1;}
				elsif ($i =~ /(cpu)/) { $hosts{$host}{$1} = &checkCPU($host); $allChecks{"cpu"} = 1;}
				elsif ($i =~ /tcp(\d+)/) { $hosts{$host}{"tcp" . $1} = &checkport($host,$1); $allChecks{"tcp" . $1} = 1;}
				else { print "unsupported input in hosts.list\n"; }
			}
		}
	}
	close HOSTS;
	
}


############################################################

sub checkAlive
{
	
	my $host = shift @_;
	$host =~ s/.+\@//;
	my $output = `/bin/ping -W 10 -c 1 $host`;
	return "fail" if $output =~ /100\% packet loss/;
	return "pass";	
}


sub checkDiskspace
{
	my $host = shift @_;
	my $output = ` $sshCmd $host df -Ph | grep \"md\\|sd\\|pool\\|data\\|xvd\\|mapper\\|uec\" | awk \'{print \$1 \"=\" \$5}\'`;
	my $output2 = ` $sshCmd $host df -h | grep \"md\\|sd\\|pool\\|data\\|xvd\\|mapper\\|uec\" | awk \'{print \$1 \"=\" \$5}\'`;
	$output = $output2 if !$output;
	$output =~ s/(\d+\%)/<font size=3 color=\"FFFFAA\">$1<\/font>/g;
	return "fail<br>$output" if $output =~ /100\%\s(?!\/media\/cd)/;
	return "pass<br><font size=2>$output</font>";
}


sub checkCPU
{
        my $host = shift @_;
		##top -n 1 |grep Cpu | cut -f 3 -d " "
        return `$sshCmd $host top -b -n 1 |grep Cpu | cut -f 3 -d \" \"`;
}


sub checkport
{
    my $host = shift @_;
    my $port = shift @_;
    $host =~ s/.+\@//;
    my $sock = new IO::Socket::INET(PeerAddr=>$host,PeerPort=>$port,Proto=>'tcp');
	return "fail" if !$sock;
    $return="pass";    
}

sub analyze
{
	my $errors;
	for $h (keys %hosts)
	{
		for $s (keys %{ $hosts{$h}})
		{
			if($hosts{$h}{$s} =~ /fail/)
			{
				$errors .= "$h: check-$s has failed\n";
			}
		}
	}
	return $errors
}

sub doFail
{
	my $errors = shift @_;
	if($errors)
	{
		my $sendmail = "/usr/sbin/sendmail -t";
		open(SENDMAIL, "|$sendmail") || die "Cannot open $sendmail: $!";
		print SENDMAIL "From: zcheck\@epiweb\n";;
		print SENDMAIL "To: ramjan\@usc.edu,shatokhi\@usc.edu\n";
		#print SENDMAIL "To: user\@usc.edu,user\@usc.edu\n";
		print SENDMAIL "Subject: Failures Detected\n\n";
		print SENDMAIL $errors;
		print STDERR $errors;
		close(SENDMAIL);
	}
}
sub resultsText
{
	for $h (sort(keys %hosts))
	{
		print STDERR "$h\n";
		for $s (keys %{ $hosts{$h}})
		{
			print STDERR "\t$s:\t\t$hosts{$h}{$s}\n"
		}
	}
}


sub resultsHTMLinline
{
	$date = `/bin/date`;
	my $content = "<html><header><title>zcheck system status</title></header><body>$date<br/>";
	$content .= "<table bgcolor=\"#000000\"><tr><td><table cellspacing=\"2\" cellpadding=\"4\" cellspacing = \"1\"><tr><td><font color=\"#FFFFFF\"><b>host</b></font></td>";
	%nicePortNames = 
	(
		tcp22 => "ssh",
		tcp80 => "www",
		tcp8080 => "tomcat",
		tcp3306 => "mysql",
		tcp445 => "samba",
		tcp636 => "LDAP-ssl",
		tcp389 => "LDAP"
	);
	for $h (sort(keys %allChecks))
	{
		if($nicePortNames{$h})
		{
			$content .= "<td><font color=\"#FFFFFF\">" . $nicePortNames{$h} . "</font></td>\n";		
		}
		else
		{
			$content .= "<td><font color=\"#FFFFFF\">$h</font></td>";
		}
	}
	$content .= "</tr>";



	for $h (sort(keys %hosts))
	{
		$content .= "<tr><td bgcolor=\"#E6E1D2\"><b>$h</b></td>";
		for $s (sort(keys %allChecks))
		{
			if(!($hosts{$h}{$s}))
			{
				$content .= "<td bgcolor=\"#779977\"><font color=\"#AAAAAA\"><center>N/A</center></font></td>\n";
			}

			elsif($hosts{$h}{$s} =~ /pass/)
			{
				$content .= "<td bgcolor=\"#008800\"><font color=\"#FFFFFF\"><center>$hosts{$h}{$s}</center></font></td>\n";
			}
			elsif($hosts{$h}{$s} =~ /fail/)
			{
				$content .= "<td bgcolor=\"#AA0000\"><font color=\"#FFFFFF\" size=6><center>$hosts{$h}{$s}</center></font></td>\n";
			}
			else
			{
				$content .= "<td bgcolor=\"#008800\"><font color=\"#FFFFFF\"><center>$hosts{$h}{$s}</center></font></td>\n";
			}
		}
		$content .= "</tr>";
		
	}
	$content .= "</table></td></tr></table></body></html>";
	return $content;
}
