#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;

$dir = $ARGV[0] || die "ecify a dir to find bams";
$cpus = $ARGV[1] || die "specify number of cpus to use";
-d $dir || die "dir not found";
$cpus =~ /^\d+$/ || die "cpus must be a number";
my $curdir = cwd();
my $bed = "/home/rcf-40/bberman/tumor/genomic-data-misc/CGIs/Takai_Jones_from_Fei_122007.fixed.PROMOTERONLY.oriented.hg19.bed";

@files = split(/\n/,`find $dir -name \"Res*.bam\" -size +1000000k`);

$tmpBase = $dir;
if($tmpBase =~ /\//)
{
	$tmpBase =~ /^.+\/(.+?)$/;
	$tmpBase = $1;
}

#split data into many pbs jobs
foreach $i  (0..$#files)
{
	$seen{basename($files[$i])} ++;
	$cmd[$i % $cpus] .= "/home/uec-00/shared/production/software/perl_utils_usc/bamToElementEnrichment.pl " . File::Spec->rel2abs($files[$i]) . " $bed > $curdir/" . basename($files[$i]) . "." . $seen{basename($files[$i])} . ".cpgRatio.metric.txt \n";
}


#create pbs files
foreach $i (0..$#cmd)
{
	my $header = "#PBS -q laird\n";
	$header .= "#PBS -l walltime=48:00:00\n";
	$header .= "#PBS -l nodes=1:ppn=8:dx340\n";
	$header .= "#PBS -d .\n";
	my $outfileName = "allBamsPBS" . $tmpBase . ".$i.sh";
	mkdir("allbams_" . $tmpBase);
	chdir("allbams_" . $tmpBase);
	open(OUT, ">$outfileName");
	print OUT $header;
	print OUT $cmd[$i];
	close OUT;
	system("qsub $outfileName");
	chdir("..");

}


