#!/usr/bin/perl
use File::Spec;
use File::Basename;
use Cwd;

$filelist = $ARGV[0] || die "specify a file to find bams";
$cpus = $ARGV[1] || die "specify number of cpus to use";
$cpus =~ /^\d+$/ || die "cpus must be a number";
my $curdir = cwd();
my $bed = "/home/rcf-40/bberman/tumor/genomic-data-misc/CGIs/Takai_Jones_from_Fei_122007.fixed.PROMOTERONLY.oriented.hg19.bed";
open(IN,"<$filelist"); 
@files = <IN>;

#split data into many pbs jobs
foreach $i  (0..$#files)
{
	chomp $files[$i];
	my $output_file = $files[$i] . ".CPGvsRandomCov.metric.txt";
	next if !-e $files[$i];
	next if ($files[$i] =~ /mdups/ || ! -z $output_file );
	$cmd[$i % $cpus] .= "/home/uec-00/shared/production/software/perl_utils_usc/bamToElementEnrichment.pl " . File::Spec->rel2abs($files[$i]) . " $bed $output_file \n";
}


#create pbs files
foreach $i (0..$#cmd)
{
	my $header = "#PBS -q laird\n";
	$header .= "#PBS -l walltime=48:00:00\n";
	$header .= "#PBS -l nodes=1:ppn=8:dx340\n";
	$header .= "#PBS -d .\n";
	my $outfileName = "cpgcov_bam" . ".$i.sh";
	mkdir("bams_inplace");
	chdir("bams_inplace");
	open(OUT, ">$outfileName");
	print OUT $header;
	print OUT $cmd[$i];
	close OUT;
	system("qsub $outfileName");
	chdir("..");

}


