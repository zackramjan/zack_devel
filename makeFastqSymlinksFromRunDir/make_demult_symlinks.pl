#!/usr/bin/perl
use File::Basename;
$usage =  "usage:\nmake_demult_symlinks.pl ../../runs/gastorage2/10102011_some_run_ABC123/Data/Intensities/BaseCalls/qseqs/demultiplexedfastqs\n";
$dir = $ARGV[0] || die $usage;

die "$dir/SamplesDirectories.csv\n sample sheet not found!" unless -e "$dir/SamplesDirectories.csv";
$ss = "$dir/SamplesDirectories.csv";

#handle the barcoded seqs
open(IN,"<$ss");
while(<IN>)
{
	chomp;
	@entry = split ',';
	#B02LRABXX,1,PEO14,Ramus_A,CGATGT,PEO14,N,R1,C,001
	@files = glob("$dir/$entry[9]/GER*/s\_$entry[1]_*sequence.txt");
	for my $f (@files)
	{			
		my $b = basename($f);
		die "symlink to $b already exists!"  if (-e $b);
		$b =~ s/sequence/$entry[4]_sequence/;
		my $cmd = "ln -s $f $b";	
		print "$entry[2]:\t$cmd\n";
		system($cmd);
	}
}

#handle the demult "unknown" folder 
for my $f (glob("$dir/unknown/GER*/s_*sequence.txt"))
{
	my $b = basename($f);
	die "symlink to $b already exists!"  if (-e $b);
	system("ln -s $f $b");	
}
