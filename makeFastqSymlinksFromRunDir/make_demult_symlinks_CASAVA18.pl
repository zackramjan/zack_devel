#!/usr/bin/perl
use File::Basename;
$usage =  "usage:\nmake_demult_symlinks.pl ../../runs/gastorage2/10102011_some_run_ABC123/Data/Intensities/BaseCalls/qseqs/demultiplexedfastqs\n";
$dir = $ARGV[0] || die $usage;

die $dir unless -d $dir;

#handle the barcoded seqs
open FOUND, "find $dir -name \"*.fastq.gz\" |";
my @files = <FOUND>;
close FOUND;
chomp @files;

for $file (@files)
{
	my @f = split("_", basename($file));
	#       ext     End    Lane  barcode

	my $lane = $f[-3];
	$lane =~ s/L|0//g;	

	my $barcode = $f[-4] ; 
	$newFile = "s_$lane\_";	

	$end2 = $file;
	$end2 =~ s/_R1_/_R2_/ if $f[-2] =~ /R1/;
	$end2 =~ s/_R2_/_R1_/ if $f[-2] =~ /R2/;
	$f[-2] =~ s/R//;
	$newFile .= "$f[-2]_" if(-e $end2); 

	$newFile .= "$barcode\_" if $f[-4] =~ /[ACTG]+/;
	$newFile .= "sequence.txt.gz";
	$cmd = "ln -s $file $newFile";
	print STDERR "$cmd\n";
	system $cmd unless $ARGV[1];
}


