#!/usr/bin/perl
use File::Basename;
##### User Specified Params
$infile = shift @ARGV || die "specify a file";
$maxDist = shift @ARGV || die "specify a max hamming distance to check for";


##### set barcodes to illumina defaults
#@availableBarcodes = qw/ATCACG CGATGT TTAGGC TGACCA ACAGTG GCCAAT CAGATC ACTTGA GATCAG TAGCTT GGCTAC CTTGTA AGTCAA AGTTCC ATGTCA CCGTCC GTAGAG GTCCGC GTGAAA GTGGCC GTTTCG CGTACG GAGTGG GGTAGC ACTGAT ATGAGC ATTCCT CAAAAG CAACTA CACCGG CACGAT CACTCA CAGGCG CATGGC CATTTT CCAACA CGGAAT CTAGCT CTATAC CTCAGA GACGAC TAATCG TACAGC TATAAT TCATTC TCCCGA TCGAAG TCGGCA/;;
@availableBarcodes = @ARGV || die "must specify codes\n";
my $failed = "unknown_barcodes";
my $multi = "multiple_barcodes";
####################


open($infh, "<$infile") || die "couldnt open $infile";
$infile = basename($infile);
while(!eof($infh))
{
        $line[0] = readline($infh); $line[1] = readline($infh); $line[2] = readline($infh); $line[3] = readline($infh);
        my $code = substr($line[0],-9,6);
        $barcodeStats{$code}++;
	my @hits = ();
	my $entry =  $line[0] . $line[1] . $line[2] . $line[3];

	for my $i (0..$maxDist)
	{
		for my $availableBarcode (@ARGV)
		{
			push @hits,  $availableBarcode if ($h = &hammingdist($code, $availableBarcode) <= $i);
		}
		last if scalar(@hits) > 0;
	}

	if(scalar(@hits) == 1)
	{
				$good++;
				my $availableBarcode = $hits[0];
				if(!($barcodes{$availableBarcode}))
				{
					#make sure we set the output file name correctly
					if($infile =~ /^(.+?_)sequence\.txt$/)
					{
						my $outfile = $1 . $availableBarcode ."_sequence.txt";
						open($barcodes{$availableBarcode},">$outfile") if(!($barcodes{$availableBarcode}));
					}
					else
					{
						open($barcodes{$availableBarcode},">$availableBarcode" . "_$infile") if(!($barcodes{$availableBarcode}));
					}
					
				}
                                my $fh =  $barcodes{$availableBarcode} || die "couldn't write";
				print $fh $entry;
	}
	if(scalar(@hits) > 1)
	{
		$ugly++;
		my $codez = join (" ", @hits);
	        print STDERR "WARNING: Read matched multiple barcodes ( $codez )!\t$line[0]" if(scalar(@hits) > 1);
                open($barcodes{$multi},">$multi" . "_$infile") if(!($barcodes{$multi}));
                my $fh =  $barcodes{$multi} || die "couldn't write";
                print $fh $entry;

	}
	if(scalar(@hits) < 1)
	{
		$bad++;
                open($barcodes{$failed},">$failed" . "_$infile") if(!($barcodes{$failed}));
                my $fh =  $barcodes{$failed} || die "couldn't write";
                print $fh $entry;
	}
}

print STDERR "matched:\t$good\nunmatched:\t$bad\nmulti match:\t$ugly\n"; 

open($summary,">summary_$infile");
foreach $key (sort {$barcodeStats{$b} <=> $barcodeStats{$a} } keys %barcodeStats)
{
     print $summary "$key $barcodeStats{$key}\n";
}

sub hammingdist{ length( $_[ 0 ] ) - ( ( $_[ 0 ] ^ $_[ 1 ] ) =~ tr[\0][\0] ) }
