#PBS -q laird
#PBS -S /bin/bash
#PBS -l walltime=48:00:00
export RESULTS_DIR=/home/uec-00/ramjan/tmp/results
export TMP=/home/uec-00/ramjan/tmp
mkdir -p $RESULTS_DIR
mkdir $TMP/$PBS_JOBID
cd $TMP/$PBS_JOBID
#COPY IN

#RUN
touch test.txt.$PBS_JOBID

#COPY OUT
cp test.txt.$PBS_JOBID $RESULTS_DIR
