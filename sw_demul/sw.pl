#!/usr/bin/perl

use List::Util qw(max);
scalar(@ARGV) > 1 || die;
$file = shift @ARGV;
@barcodes = @ARGV;
my $readnum = 0;
my $outputfile;

push @barcodes, "UNKNOWN";
for my $barcode (@barcodes)
{
	my $outfile;
	if($file =~ /s_(\d)_sequence.txt/)
	{
		$outfile = "s_$1\_$barcode\_sequence.txt";
	}
	elsif($file =~ /s_(\d)_(\d)_sequence.txt/)
	{
		$outfile = "s_$1\_$2\_$barcode\_sequence.txt";
	}
	else
	{
		$outfile = "$barcode\_$file";
	}
	open(my $fh, ">$outfile");
	$outputfile{$barcode} = $fh;

}
pop @barcodes;


open(IN,"<$file");
while(my $line = <IN>)
{
	my $line2 = <IN>;
	my $line3 = <IN>;
	my $line4 = <IN>;
	if($line =~ /^\@.+#(\w\w\w\w\w\w)\/\d$/)
	{
		$readnum++;
		my $query = $1;
		my $match = &findcode($query);
		my $fh = $outputfile{$match};
		$queryCount{$query}++;
		$barcodeCount{$match}++;
		$placedCount{"$query - $match"}++;
		print  $fh "$line$line2$line3$line4";


	}
	print STDERR ($readnum / 4000000) . " million reads processed\n" if ($readnum % 4000000 == 0);
}
print STDERR "$cacheMisses alignemnts performed, " . ($readnum - $cacheMisses) . " where cached\n";

&stats();

sub stats
{
	print "\nTop barcodes occuring in the file\n"; 
	&printstats(\%queryCount,20);	

	print "\nDistribution of illumina barcodes after matching analysis\n"; 
	&printstats(\%barcodeCount,50);	

	print "\nTop file-barcode - illumina-barcode matches\n"; 
	&printstats(\%placedCount,50);	
	sub printstats
	{
		my %hash = %{$_[0]};
		my $max = $_[1];
		my $i = 1;
		foreach my $value (sort {$hash{$b} <=> $hash{$a} } keys %hash)
		{
			print "$i\t$value:\t$hash{$value}\n" ;
			$i++;
			return if $i > $max;
		}


	}

}


sub findcode
{
	my $query = shift @_;
	return $cached{$query} if $cached{$query};
	$cacheMisses++;
	my %scores;
	$scores{smwa($query,$_)} .= $_ foreach @barcodes;
	my $maxkey = max(keys %scores);

	if(length($scores{$maxkey}) < 10 && $maxkey > 3.0)
	{
		#print STDERR "$query - $scores{$maxkey}\t$maxkey\n";
		$cached{$query} = $scores{$maxkey};
		return $scores{$maxkey}
	}
	else
	{
		#print STDERR "$query - $scores{$maxkey}\t$maxkey BAD MATCH OR NOT UNIQUE\n";
		$cached{$query} = "UNKNOWN";
		return "UNKNOWN";
	}
}

sub smwa
{
	# Smith-Waterman  Algorithm

	# get sequences from command line
	my ($seq1, $seq2) = @_;

	# scoring scheme
	my $MATCH    =  1; # +1 for letters that match
	my $MISMATCH = -.75; # -1 for letters that mismatch
	my $GAP      = -.75; # -1 for any gap

	# initialization
	my @matrix;
	$matrix[0][0]{score}   = 0;
	$matrix[0][0]{pointer} = "none";
	for(my $j = 1; $j <= length($seq1); $j++) {
	    $matrix[0][$j]{score}   = 0;
	    $matrix[0][$j]{pointer} = "none";
	}
	for (my $i = 1; $i <= length($seq2); $i++) {
	    $matrix[$i][0]{score}   = 0;
	    $matrix[$i][0]{pointer} = "none";
	}

	# fill
	my $max_i     = 0;
	my $max_j     = 0;
	my $max_score = 0;

	for(my $i = 1; $i <= length($seq2); $i++) {
	    for(my $j = 1; $j <= length($seq1); $j++) {
		my ($diagonal_score, $left_score, $up_score);
		
		# calculate match score
		my $letter1 = substr($seq1, $j-1, 1);
		my $letter2 = substr($seq2, $i-1, 1);       
		if ($letter1 eq $letter2) {
		    $diagonal_score = $matrix[$i-1][$j-1]{score} + $MATCH;
		}
		else {
		    $diagonal_score = $matrix[$i-1][$j-1]{score} + $MISMATCH;
		}
		
		# calculate gap scores
		$up_score   = $matrix[$i-1][$j]{score} + $GAP;
		$left_score = $matrix[$i][$j-1]{score} + $GAP;
		
		if ($diagonal_score <= 0 and $up_score <= 0 and $left_score <= 0) {
		    $matrix[$i][$j]{score}   = 0;
		    $matrix[$i][$j]{pointer} = "none";
		    next; # terminate this iteration of the loop
		}
		
		# choose best score
		if ($diagonal_score >= $up_score) {
		    if ($diagonal_score >= $left_score) {
			$matrix[$i][$j]{score}   = $diagonal_score;
			$matrix[$i][$j]{pointer} = "diagonal";
		    }
		    else {
			$matrix[$i][$j]{score}   = $left_score;
			$matrix[$i][$j]{pointer} = "left";
		    }
		} else {
		    if ($up_score >= $left_score) {
			$matrix[$i][$j]{score}   = $up_score;
			$matrix[$i][$j]{pointer} = "up";
		    }
		    else {
			$matrix[$i][$j]{score}   = $left_score;
			$matrix[$i][$j]{pointer} = "left";
		    }
		}
		
		# set maximum score
		if ($matrix[$i][$j]{score} > $max_score) {
		    $max_i     = $i;
		    $max_j     = $j;
		    $max_score = $matrix[$i][$j]{score};
		}
	    }
	}

	# trace-back
	return $max_score;
	my $align1 = "";
	my $align2 = "";

	my $j = $max_j;
	my $i = $max_i;

	while (1) {
	    last if $matrix[$i][$j]{pointer} eq "none";
	    
	    if ($matrix[$i][$j]{pointer} eq "diagonal") {
		$align1 .= substr($seq1, $j-1, 1);
		$align2 .= substr($seq2, $i-1, 1);
		$i--; $j--;
	    }
	    elsif ($matrix[$i][$j]{pointer} eq "left") {
		$align1 .= substr($seq1, $j-1, 1);
		$align2 .= "-";
		$j--;
	    }
	    elsif ($matrix[$i][$j]{pointer} eq "up") {
		$align1 .= "-";
		$align2 .= substr($seq2, $i-1, 1);
		$i--;
	    }   
	}

	$align1 = reverse $align1;
	$align2 = reverse $align2;
	print "$align1\n";
	print "$align2\n";

}
