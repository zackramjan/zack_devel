#!/usr/bin/perl
use File::Basename;
$i=0; 
$g=1; 

my @dirsf = split("/",$ARGV[1]);
my $output = $dirsf[-3];
$output =~ s/\.|\-//g;
$output = "workFlowParams_$output";

for $file (@ARGV)
{
	if($i % 16==0)
	{
		open(OUT,">$output\.$g\.txt");
		my @dirsf = split("/",$ARGV[1]);
		print OUT getHeader($dirsf[-3]);
		$g++;
	}
	my $input = $file;

	die  "file not found" unless -f $file;
	my($name, $dir, $suffix) = fileparse($file,(".txt",".fastq",".fastq.txt",".bam"));

	if($name =~ /^(.+)1$/) 
	{
		my $input2 = "$1"."2$suffix";
		$input .= ",$input2" if -e $input2;
	}
	$name =~ s/.\d$//;

	my @dirs = split("/",$file);
	#$name = "$dirs[-2]$dirs[-1]";
	$name = $dirs[-2];
	my $lane = $dirs[-1];
	$lane =~ /s_(\d)_/;
	$lane = $1;
	#$name =~ s/\_sequence\.txt$//;
	#$name =~ s/s_//;
	print OUT getParam($name,++$i,$input,$lane);
}

sub getParam
{
	my ($NAME,$N,$INPUTS,$LANE) = @_;
	my $param = "#Sample: $NAME\nSample.$N.SampleID = $NAME\nSample.$N.Lane = $LANE\nSample.$N.Input = $INPUTS\nSample.$N.Workflow = bisulfite\nSample.$N.Reference = /home/uec-00/shared/production/genomes/hg19_rCRSchrm/hg19_rCRSchrm.fa\n\n";
	return $param;
}

sub getHeader
{
	my $flowcell = shift @_;;
	$flowcell =~ s/\.|\_//g;
	return "ClusterSize = 1\nqueue = laird\nFlowCellName = $flowcell\nMinMismatches = 2\nMaqPileupQ = 30\nreferenceLane = 1\nrandomSubset = 300000\n\n";
}
