#!/usr/bin/perl
use File::Basename;
$i=1; 
$j=1; 
$max = 193;

open(IN,"$ARGV[0]");
open(OUT,">$ARGV[1]\.$j\.txt");

print OUT getHeader();

while( $line=<IN> )
{
	my @entry = split(",",$line);
	next if $line !~ /\d/;
	if($i % $max == 0)
	{
		close OUT;
		$j++;
		open(OUT,">$ARGV[1].$j.txt");
		print OUT getHeader();
	}
	print OUT getParam($entry[2],$i++, "$entry[2]\_$entry[4]\_L00$entry[1]\_R1_001.fastq.gz,$entry[2]\_$entry[4]\_L00$entry[1]\_R2_001.fastq.gz",$entry[1]);
}

sub getParam
{
	my ($NAME,$N,$INPUTS,$LANE) = @_;
	my $param = "#Sample: $NAME\nSample.$N.SampleID = $NAME\nSample.$N.Lane = $LANE\nSample.$N.Input = $INPUTS\nSample.$N.Workflow = minimal\nSample.$N.Reference = /home/uec-00/shared/production/genomes/hg19_rCRSchrm/hg19_rCRSchrm.fa\n\n";
	return $param;

}

sub getHeader
{
	return "ClusterSize = 1\nqueue = laird\nFlowCellName = ANALYSIS\nMinMismatches = 2\nMaqPileupQ = 30\nreferenceLane = 1\nrandomSubset = 300000\n\n";

}
