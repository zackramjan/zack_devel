#!/usr/bin/perl
use File::Basename;
$i=1; 
$j=0;
$max=1;
my $FH;

my $paramFile = shift @ARGV;
die "must specify a param file as first arg" if -e $paramFile;


for $file (@ARGV)
{
	
	my $input = $file;
	nextFile();

	die  "file not found" unless -f $file;
	print STDERR "creating workflow for $file\n";
	my($name, $dir, $suffix) = fileparse($file,(".txt",".fastq",".fastq.txt",".fastq.gz",".bam"));
	if($name =~ /1/) 
	{
		my $input2 = $input;
		$input2 =~ s/R1/R2/ if $input2 =~ /_R1[\._]/;
		$input2 =~ s/1(\D+)$/2$1/ if $input !~ /R1/;

		$input .= ",$input2" if -e $input2 && $input ne $input2;
	}
	my @tmp = split("_",$name);
	$name =~ s/[\-_\.]//g;
	print $FH getParam("$tmp[0]$tmp[1]",$i++,$input);
}

sub getParam
{
	my ($NAME,$N,$INPUTS) = @_;
	my $CLEAN_NAME = $NAME;
	$CLEAN_NAME =~ s/\W+//g;
	$CLEAN_NAME =~ s/L00\d+//;
	my $param = "#Sample: $NAME\nSample.$N.SampleID = $CLEAN_NAME\nSample.$N.Lane = 1\nSample.$N.Input = $INPUTS\nSample.$N.Workflow = minimal\nSample.$N.Reference = /home/uec-00/shared/production/genomes/encode_hg19_mf/male.hg19.fa\n\n";
	return $param;

}

sub getHeader
{
	my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,$atime,$mtime,$ctime,$blksize,$blocks) = stat("./") or die $!;
	#$uid = getpwuid($uid);
	$uid = basename(`pwd`);
	return "ClusterSize = 1\nqueue = laird\nFlowCellName = $uid\nMinMismatches = 2\nMaqPileupQ = 30\nreferenceLane = 1\nrandomSubset = 300000\n\n";

}

sub nextFile
{
	if($i % $max == 0 || $i==1)
	{
		$j++;
		open($FH,">$paramFile\.$j\.txt");
		print STDERR "writing next $max to $paramFile\.$j\.txt\n";
		print $FH getHeader();
	}

}
