#!/usr/bin/perl
use File::Basename;
use File::Spec;
@ARGV = map {File::Spec->rel2abs($_)} @ARGV;
print getHeader();
-f $_ || die for @ARGV;
print getParam("MERGE",1,join(",",@ARGV));

sub getParam
{
	my $i=1; 
	my ($NAME,$N,$INPUTS) = @_;
	my $param = "#Sample: $NAME\nSample.$N.SampleID = $NAME\nSample.$N.Lane = 1\nSample.$N.Input = $INPUTS\nSample.$N.Workflow = bismerge\nSample.$N.Reference = /home/uec-00/shared/production/genomes/hg19_rCRSchrm/hg19_rCRSchrm.fa\n\n";
	return $param;

}

sub getHeader
{
	return "ClusterSize = 1\nqueue = laird\nFlowCellName = MERGERUNS\nMinMismatches = 2\nMaqPileupQ = 30\nreferenceLane = 1\nrandomSubset = 300000\n\n";

}
