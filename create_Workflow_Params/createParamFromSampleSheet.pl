#!/usr/bin/perl
use File::Basename;
$i=1; 
print getHeader();

open(IN,"$ARGV[0]");

while( $line=<IN> )
{
	my @entry = split(",",$line);
	next if $line !~ /\d/;
	print getParam($entry[2],$i++, "s_$entry[1]\_1_$entry[4]\_sequence.txt,s_$entry[1]\_2_$entry[4]\_sequence.txt");
}

sub getParam
{
	my ($NAME,$N,$INPUTS) = @_;
	my $param = "#Sample: $NAME\nSample.$N.SampleID = $NAME\nSample.$N.Lane = 1\nSample.$N.Input = $INPUTS\nSample.$N.Workflow = minimal\nSample.$N.Reference = /home/uec-00/shared/production/genomes/hg19_rCRSchrm/hg19_rCRSchrm.fa\n\n";
	return $param;

}

sub getHeader
{
	return "ClusterSize = 1\nqueue = laird\nFlowCellName = ANALYSIS\nMinMismatches = 2\nMaqPileupQ = 30\nreferenceLane = 1\nrandomSubset = 300000\n\n";

}
