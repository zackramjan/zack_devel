#!/usr/bin/perl
require LWP::UserAgent;


for my $line (split("\n",getMap()))
{
	my ($arrayID,$location,$sample) = split("\t",$line);

	#die on duplicate arrays in geneus 
	$errors{"dupArrays"} .= "WARNING: duplicate entry for ARRAY ID:\t $arrayID:$location\n" if $checkDuplicates{$arrayID . $location};
	$checkDuplicates{$arrayID . $location} .= "$sample,";	

	#die on duplicate tcga id
	$errors{"dups"} .= "WARNING: TCGA ID on multiple arrays:\t $sample on $arrayID,$checkDuplicates{$sample}\n" if $sample =~ /tcga/i && $checkDuplicates{$sample} && $checkDuplicates{$sample} !~ /$arrayID/;
	$checkDuplicates{$sample} .= "$arrayID,";	

	#malformed names
	$errors{"malformed"} .= "WARNING: TCGA ID name does not conform:\t $sample\n" if $sample =~ /tcga/i && $sample !~ /TCGA-\w+-\w+-\w+-\w+-\w+-\w+/i;
	
	#diff samples on the same plate and location, BAD!
	$sampleRE = quotemeta($sample);
	$errors{"dupSamp"} .= "ERROR: Diff Sample on same ArrayID:\t $arrayID:$location has $sample,$checkDuplicates{$arrayID . $location}\n" if $checkDuplicates{$arrayID . $location} && $checkDuplicates{$arrayID . $location} !~ /$sampleRE/;
	

	print "$line\n";
	
}

print STDERR $errors{"malformed"};
print STDERR $errors{"dupArrays"};
print STDERR $errors{"dups"};
print STDERR $errors{"dupSamp"};



sub getMap
{
        my $ua = LWP::UserAgent->new;
        $ua->credentials("webapp.epigenome.usc.edu:80","webapp.epigenome.usc.edu:80",'automationbot'=>'autoBoT!');
        my $response = $ua->get("http://webapp.epigenome.usc.edu/gareports/beadmap.jsp");
        my $ret = $response->content;
        return $ret;
}
