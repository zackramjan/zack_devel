#!/usr/bin/perl 

use POSIX;
use Term::Cap;
use Time::Local;
use Term::ANSIColor;
my $screen_width = 220;
my $screen_height = 40;
my $delay = .001;
open (IN, "<input.sam");
    
init();                     # Initialize Term::Cap.
zip();                      # Bounce lines around the screen.
finish();                   # Clean up afterward.
exit();


# Bounce lines around the screen until the user interrupts with
# Ctrl-C.
sub zip { 
    clear_screen();
    ($maxrow, $maxcol) = ($tcap->{_li} - 1, $tcap->{_co} - 1);


    $interrupted = 0;
    $SIG{INT} = sub { ++$interrupted };

	while(!$interrupted )
	{
		&populate();
		for my $i (0..$screen_height)
		{
			if($data[$i][6] == 0)
			{
				gotoxy($data[$i][4],$i );
				print $data[$i][0] if  $data[$i][4] > 0;
				$data[$i][4] -- if $data[$i][4] > 0 ;
				#if($data[$i][4] == 0)	
				#{
				#	gotoxy($data[$i][5],$i );
				#	print  color("WHITE") . "Aligning to " . color("RED") . "HUMAN" . color("WHITE") . " genome at " . color("CYAN","BOLD") . "$data[$i][2]:$data[$i][3]" .  color("black") ." " . color("reset") if  $data[$i][5]  >= 1;
				#	gotoxy($data[$i][5],$i );
				#	print  color("WHITE") . "Aligned to  " . color("RED") . "HUMAN" . color("WHITE") . " genome at " . color("RED","ON_YELLOW","BOLD") . "$data[$i][2]:$data[$i][3]" .  color("black","ON_BLACK") ." " . color("reset") if $data[$i][5]  <= 1 && !$ARGV[0];
				#	$data[$i][5]-- if $data[$i][5] > 0 ;
				#}
				#delete $data[$i] if  $data[$i][5]  == 0;
				delete $data[$i] if  $data[$i][4]  == 0;
				select(undef, undef, undef, $delay);
				}
			else
			{
				$data[$i][6] --;
			}
		}
	}
}



sub populate
{
	
	for my $i (0..$screen_height)
	{
		if(!$data[$i])
		{
			my @line = split("\t",<IN>);
			my $colored = color("reset") . color("BOLD")  . "                                         "; 
			for my $x (split  "", $line[9])
			{
				if(!$ARGV[0])
				{
				#	$colored .= color("UNDERLINE");
					$colored .= (color("red") . "T" )  if $x eq "T";
					$colored .= (color("green") . "A" )  if $x eq "A";
					$colored .= (color("blue") . "C" )  if $x eq "C";
					$colored .= (color("yellow") . "G" )  if $x eq "G";
				}
				else
				{
					$colored .= (color("on_red","black") . "A" )  if $x eq "A";
					$colored .= (color("on_green","black") . "C" )  if $x eq "C";
					$colored .= (color("on_blue","black") . "T" )  if $x eq "T";
					$colored .= (color("on_yellow","black") . "G" )  if $x eq "G";
				}
			}
			$colored .= color("black","ON_BLACK") . " " . color("reset");
			@{$data[$i]} = ($colored,$line[9], $line[2] ,$line[3],$screen_width,$screen_width,int(rand($screen_width)));
		}
	}

}





# Two convenience functions.  clear_screen is obvious, and
# clear_end clears to the end of the screen.
sub clear_screen { $tcap->Tputs('cl', 1, *STDOUT) } 
sub clear_end    { $tcap->Tputs('cd', 1, *STDOUT) } 

# Move the cursor to a particular location.
sub gotoxy {
    my($x, $y) = @_;
    $tcap->Tgoto('cm', $x, $y, *STDOUT);
} 

# Get the terminal speed through the POSIX module and use that
# to initialize Term::Cap.
sub init { 
    $| = 1;
    my $termios = POSIX::Termios->new();
    $termios->getattr;
    my $ospeed = $termios->getospeed;
    $tcap = Term::Cap->Tgetent ({ TERM => undef, OSPEED => $ospeed });
    $tcap->Trequire(qw(cl cm cd));
}

# Clean up the screen.
sub finish { 
    gotoxy(0, $maxrow);
    clear_end();
}

	
