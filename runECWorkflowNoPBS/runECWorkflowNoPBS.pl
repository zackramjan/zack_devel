#!/usr/bin/perl
my $i = 1;
for my $param (@ARGV)
{
	runcmd("java -Xms4096m -Xmx4096m -cp /home/uec-00/shared/production/software/ECWorkflow/ECWorkFlow.v4.jar:/home/uec-00/shared/production/software/ECWorkflow/pegasus.jar edu.usc.epigenome.workflow.SequencingPipeline -pbs -dryrun $param");
	runcmd("mv dryrun.log.txt $param.dryrun.log.txt");
	open my $out, ">$param.run.sh";
	
	print $out "#!/bin/bash\nexport PBS_JOBID=NOPBS$i\n";
	$i++;

	open(IN, "<$param.dryrun.log.txt");
	while(<IN>)
	{
		#print $_ unless $foundPathLine && $_ =~ /export PATH/;
		print $out $_ unless ($foundPathLine && $_ =~ /export PATH/) || $_ =~ /^\#\w+/;
		$foundPathLine = 1 if $_ =~ /export PATH/
	}
	close $out;
	runcmd("chmod +x $param.run.sh");
}


sub runcmd{
	my $cmd=shift @_;
	my $caller=(caller(1))[3];
	print STDERR "$caller\t$cmd\n";
	system($cmd);
}
