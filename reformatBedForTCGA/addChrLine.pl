#!/usr/bin/perl
$file = $ARGV[0] || die;
$output = `head -n 1 $file | grep fields`;
die "$file already has field line in header\n" if $output;
system("sed -i \"1s/\$/ fields=\\\"chrom,chromStart,chromEnd,name,score,strand,percentMeth,numCTreads\\\"/\" $file");
