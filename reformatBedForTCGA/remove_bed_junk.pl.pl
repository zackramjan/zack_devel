#!/usr/bin/perl
#
#
my $file = $ARGV[0];
open(IN,"<$file");

my $outfile = $file;
$outfile =~ s/bed$//;
$outfile .= "fixed.bed";
die if -e $outfile;
open(OUT,">$outfile");

while ($line = <IN>)
{
	$line =~ s/NA/./g if $line !~ /track/;
	my @out = split(/\t/,$line);
	$out[3] = "." if $line !~ /track/;
	my $newline = join("\t",@out);
	print OUT $newline;
}
