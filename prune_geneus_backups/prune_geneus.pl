#!/usr/bin/env perl
use POSIX;
use strict;

my %oldFiles;

for my $file (glob("*.[bg]z*"))
{
	my $dateKey = POSIX::strftime( "%y-%m", localtime((stat $file)[9]));	
	
	push(@{$oldFiles{$dateKey}}, $file) if -M $file > 45 ;
}


foreach my $month (sort keys %oldFiles) 
{
	print "$month\n";	
	my @files = sort @{$oldFiles{$month}};

	my @todelete;
	foreach (0..$#files)
	{
		push(@todelete, $files[$_]) if ($_ % 7 != 0 && $#files > 6);
	}
	 
	foreach my $file (@todelete) 
	{
		mkdir "old";
		print "\tmv $file old/\n";
		system "mv $file old/";
	}	  

}
