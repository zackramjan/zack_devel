#!/usr/bin/perl
$file = $ARGV[0];
open(IN, "< $file") || die "filenotfound";

$line = 0;
while(<IN>)
{

	$line++;
	countSeq($_) if $line % 4 == 2;
	countQual($_) if $line % 4 == 0;
}


for my $x (0..$#bc)
{
	print "cycle " . ($x + 1) . " \t";
	my $ref = $bc[$x];
	for my $y (sort { $a <=> $b} keys(%$ref))
	{
		print "$y=" . $ref->{$y} . "\t";	
	}
	print "\n";
}

for my $x (0..$#qc)
{
	print "cycle " . ($x + 1) . " \t";
	my $ref = $qc[$x];
	for my $y (sort {$a <=> $b} keys(%$ref))
	{
		print "q$y=" . $ref->{$y} . "\t";	
	}
	print "\n";
}

sub countSeq
{
	my $seq = shift @_;
	#print "S=$seq\n";
	@nucs = split(//,$seq);
	for my $i (0..$#nucs)
	{
		$bc[$i]{$nucs[$i]}++;
	}	
	
}

sub countQual
{
	my $qual = shift @_;
	#print "Q=$qual\n";
	@quals = split(//,$qual);
	for my $i (0..$#quals)
	{
		$qc[$i]{ord($quals[$i]) - 64}++;
	}	
}

