#!/usr/bin/perl
use Time::Local;
use Term::ANSIColor;


$output = `pbsnodes :laird`;
$zstatout = `~ramjan/bin/zstat.pl all`;
$htmlMode = $ARGV[0] if $ARGV[0] =~ /html/i;
foreach my $line (split '\n', $zstatout)
{
	chomp $line;
	$line =~ s/[\000-\037]\[(\d|;)+m//g;;
	$line =~ s/^\s+//;
	$line =~ s/\s+/\|/g;
	my @z = split(/\|/, $line);
	$zstat{$z[0]} = [@z];
}

@nodes = split "\n\n", $output;

foreach  $n (@nodes)
{
	my @lines = split "\n", $n;
	$node = $lines[0];
	chomp $node;
	for $l (@lines)
	{
		if( $l =~ /\s*(\S*)\s+\=\s+(.+)$/)
		{
			$nodeprops{$node}{$1} = $2;

		}
	}

}

$date = `date`;
print "<html><body bgcolor='black'><font color='white'>last update at $date<font><table>" if $htmlMode;


foreach $k (sort keys %nodeprops)
{
	my %nodejobs;
	$color = $nodeprops{$k}{jobs} =~ /s+/ ? "red" : "green";
	$nodeprops{$k}{jobs} =~ s/\-\d+.hpc-pbs.hpcc.usc.edu//g;
	$nodeprops{$k}{jobs} =~ s/.hpc-pbs.hpcc.usc.edu//g;
	$nodeprops{$k}{jobs} =~ s/\d+\///g;
	$nodeprops{$k}{jobs} =~ s/ //g;
	$nodejobs{$_}++ for split ",",$nodeprops{$k}{jobs};

	if($htmlMode)
	{
		print "<tr>\n";
		print "\t<td><font color='$color'> $k </font></td>\n";
		print "\t<td><font color='white'>$nodeprops{$k}{np}core</font></td>\n";
		print "\t<td>";
		print "<font color='white'>[</font><font color='blue'>$_</font><font color='white'>:$nodejobs{$_}cores:</font><font color='yellow'>$zstat{$_}[2]</font><font color='white'>]</font>" for  (keys %nodejobs);
		print "<font color='green'>FREE</font>\n"  if !%nodejobs;
		print "</td>\n";
		print "</tr>\n";
	}
	else
	{
		print color($color) . "$k    " . color("reset");
		print "$nodeprops{$k}{np}core\t";
		print "[" . color("blue") . "$_" . color("reset") . ":$nodejobs{$_}cores:" . color("yellow") . "$zstat{$_}[2]" . color("reset") . "]" for  (keys %nodejobs);
		print color("green") . "FREE" .  color("reset") if !%nodejobs;
		print "\n";
	}
}
print "</table></body></html>\n" if $htmlMode;
