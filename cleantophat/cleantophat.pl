#!/usr/bin/perl
use strict;
use File::Basename;

my $dir = $ARGV[0] || die "what dir to look in?";
-d $dir || die "must be a dir";
open FOUND, "find $dir -name \"*tophat*sorted.bam.reorder.bam\" |";



while(my $tbam = <FOUND>)
{
	chomp $tbam;
	my $dirname  = dirname($tbam);
	my @bams = glob("$dirname/*mdups.bam");
	my $bam = pop @bams;

		
	if(-e $tbam && -e $bam && -s $tbam <= -s $bam && $tbam ne $bam && $tbam !~ /hpc-pbs/)
	{
		my @badbams = glob("$dirname/*tophat_hits.bam*");
		print "rm $_\n" for (@badbams);
		system("rm $_") for (@badbams);
		print "Keep $bam\n\n"; 
	}
	elsif(-e $bam && -e $tbam)
	{
		print "SKIPPING: $bam $tbam\n";
	}
}
