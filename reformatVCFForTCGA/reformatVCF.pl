#!/usr/bin/perl
use File::Basename;
use File::Path;
use Cwd;

my $TOOLDIR = "/auto/uec-00/shared/production/software/tcga/submissionPrepTools/level2and3";
open(SAMPLES,"<$TOOLDIR/samples.csv");


while(<SAMPLES>)
{
	chomp;
	my ($status, $timestamp, $center, $sample_barcode, $sample_uuid, $disease, $platform, $genome, $analysis_uuid, $bam, $geneus_id)  = split(/\t/);
	print STDERR "processing $sample_barcode\n";
	#live	2012-04-20T22:01:45Z	USC-JHU	TCGA-A2-A04X-01A-21D-A19F-05	abff5667-7ef4-46c1-ad34-ceabc27e7992	BRCA	Bisulfite-Seq	HG19	5bcd1c9d-798c-4206-813c-54cdda797d21	TCGA-BRCA-A04X-TP-21D-A19F-05_5c0eabc4-fb55-463e-ae0f-a25ae18cc78aD.bam NIC123

	print STDERR "Already processed newer submission of $sample_barcode, skipping older one\n" if $seen{$sample_barcode};
	next if $seen{$sample_barcode};
	$sample_uuid = getUUID($sample_barcode);
	
	my $bambase = $bam;
	$bambase =~ s/\.bam$//;
	my ($vcf) = glob("$bambase*MERGED.vcf");


	die "cant find $vcf \n"  unless $vcf;

	my $sampleLine =  "##SAMPLE=<ID=$geneus_id,SampleUUID=$sample_uuid,SampleTCGABarcode=$sample_barcode,MetadataResource=NA,File=$bam,Platform=Illumina,Source=CGHub,Accession=$analysis_uuid>\n";
	print STDERR $sampleLine;
	print STDERR "$vcf\n\n\n";
	
	fixVCF($vcf,$sampleLine);

	$seen{$sample_barcode} = $sample_barcode;
}

sub fixVCF
{
	my $vcf = shift @_;
	my $sampleLine = shift @_;
	
	open (IN, "<$vcf");
	my $out = $vcf;
	$out =~ s/vcf//;
	$out .= "ID.vcf";
	my $found = 0;
	
	die if -e $out;
	open(OUT,">$out");

	while (my $line = <IN>)
	{
		print OUT $sampleLine if $line =~ /^#CHROM\tPOS/;
		$found = 1 if $line =~ /^#CHROM\tPOS/;
		print OUT $line; 
	}
	die "SAMPLE $vcf LINE NOT INSERTED" if  $found == 0;



}

sub getUUID
{
	$UUID_URL = "https://tcga-data.nci.nih.gov/uuid/uuidws/mapping/xml/barcode";
        return $givenUUID if $givenUUID;
        my $tcgaBarcode = shift @_;
        print STDERR "UUID Lookup: curl $UUID_URL/$tcgaBarcode\n";
        my $output = `curl $UUID_URL/$tcgaBarcode 2> /dev/null`;
        print STDERR "UUID Lookup: $output";
        $output =~ /\<uuid\>(.+?)\<\/uuid\>/;
        my $tcgaUUID =  $1;
        print STDERR "UUID Lookup: $tcgaBarcode:$tcgaUUID\n";
	die "uuid failed " unless $tcgaUUID;
        return $tcgaUUID ;
}

