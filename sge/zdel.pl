#!/usr/bin/perl
#Zack Ramjan - USC Epigenome Center
#PBS HACK, wrap pbs qdel to look like sge qdel with emulation of array jobs
#by using a global jobid and unique task ID's in the job name
#in the form
#name = zXXXX_YYY where XXX is the jobid and YYY is the task id
# delete all that match  XXX in the name by iterating through the jobs of this user

use Time::Local;
use strict;
my $job_to_delete = shift @ARGV || die "no job to delete specified";

my $PBS_QSTAT = "qstat -f";
my $PBS_QDEL = "qdel";
my $username = $ENV{USER};

#FOR TESTING ONLY
$PBS_QSTAT = "ssh ramjan\@hpc-login2 qstat -f";
$username = "ramjan";

my $pbsQstat = `$PBS_QSTAT`;
my @pbsJobs = split("Job Id:", $pbsQstat);

for my $job (@pbsJobs)
{

        my %jobDetails;
        my @jobLines = split("\n", $job);
        chomp $jobLines[0];
	if($jobLines[0] =~ /\s*\d+/)
	{
		$jobDetails{"Job Id"} = $jobLines[0];
		for my $jobProp (@jobLines)
		{
			if($jobProp =~ /\s*(\w+)\s=\s(.+)$/)
			{
				$jobDetails{$1} = $2;
				chomp $jobDetails{$1};
			}
		}


		#clean up pbs output to conform to SGE
		# to simulate arrays we mangle the job names and use it as an id and task
		#JOB ID
		$jobDetails{"Job Id"} =~ /^\s*(\d+)/;
		$jobDetails{"Job Id"} = $1;
		$jobDetails{"Real_Id"} = $jobDetails{"Job Id"};
		
		my $jobid = my $taskid = "";
		if($jobDetails{"Job_Name"} =~ /z(\d+)_(\d+)/)  
		{
			$jobDetails{"Job_Name"} = $1;
			$jobDetails{"Task_Id"} = $2;
		}

		#JOB STATE
		$jobDetails{"job_state"} = lc($jobDetails{"job_state"});
		$jobDetails{"job_state"} =~ s/q/qw/; 

		#JOB START TIME
		my %months =	("Jan"=>1, "Feb"=>2, "Mar"=>3, "Apr"=>4, "May"=>5, "Jun"=>6, 
				"Jul"=>7, "Aug"=>8, "Sep"=>9, "Oct"=>10, "Nov"=>11, "Dec"=>12);
		my @time = split(/\s+/,  $jobDetails{"ctime"}); 
		$jobDetails{"ctime"} = "$months{$time[1]}/$time[2]/$time[4] $time[3]";

		#JOB OWNER
		$jobDetails{"Job_Owner"} =~ s/\@.+$//; 

		#delete job
		if ($jobDetails{"Job_Owner"} eq $username && $jobDetails{"Job Id"} eq $job_to_delete)
		{
			system("qdel " . $jobDetails{"Real_Id"});
		}

	}
}
