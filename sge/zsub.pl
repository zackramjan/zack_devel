#!/usr/bin/perl
#Zack Ramjan - USC Epigenome Center
#PBS HACK, wrap pbs qsub to look like sge qstat. we emulate array jobs
#by using a global jobid and unique task ID's in the job name
#in the form
#name = zXXXX_YYY where XXX is the jobid and YYY is the task id
#we print this psuedo job id to stdout

$jobid = time ;
$jobid = substr $jobid,2;
$returnJobid = $jobid;
$jobid = "z$jobid" . "_"  ;

for $i (1..10)
{
	`qsub -N $jobid$i -v SGE_TASK_ID=$i -q laird -S /bin/bash test.sh`;	
}
print "Your job-array $returnJobid.$i has been submitted\n";
