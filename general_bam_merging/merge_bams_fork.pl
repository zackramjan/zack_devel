#!/usr/bin/perl
use File::Spec;
use File::Basename;

my $SAMDIR = "/home/uec-00/shared/production/software/samtools";
my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";

my $output = shift @ARGV;
my $library = "Unspecified Library";
$library = shift @ARGV if(!-e $ARGV[0]);

#add readgroups

my $i = 1;
for my $file (@ARGV)
{
	my $pid=fork();
	push @childs, $pid if($pid != 0);
	if($pid == 0)	
	{
		sleep $i;
		my $fullpath = File::Spec->rel2abs( $file ) ;
		my $filebase = basename($file);
		my $date = `date`;
		chomp $date;
		#get flowcell and lane
		$file =~ /_(\w\w\w\w+XX)_s?_?(\d+)/;
		my $fc = $1 || "Flowcell$i";
		my $ln = $2 || "L$i";

		runcmd("$JAVA -Xmx2g -jar $PICARD/AddOrReplaceReadGroups.jar VALIDATION_STRINGENCY=SILENT MAX_RECORDS_IN_RAM=500000 INPUT='$file' OUTPUT='with_rg_$filebase' RGID='$fc\.$ln' RGLB='$library' RGPL='illumina' RGPU='$fc\.$ln' RGSM='$library' RGCN='USC EPIGENOME CENTER' RGDS='from file $fullpath on $date'");
		exit 0;
	}
	$i++;
}
foreach (@childs) 
{
	my $tmp = waitpid($_, 0);
	print "done with pid $tmp\n";

}



#merge
my $cmd = "VALIDATION_STRINGENCY=SILENT MERGE_SEQUENCE_DICTIONARIES=true CREATE_INDEX=true USE_THREADING=true MAX_RECORDS_IN_RAM=7000000 OUTPUT='$output' ";
for my $file (@ARGV)
{
	my $filebase = basename($file);
	$cmd .= "INPUT='with_rg_$filebase' ";
}

runcmd("$JAVA -Xmx22g -jar $PICARD/MergeSamFiles.jar $cmd");
sub runcmd
{
        my $cmd = shift @_;
        print STDERR "$cmd\n\n";
        system($cmd);
}
