#!/usr/bin/perl
use File::Spec;
use File::Basename;
use File::Find;
use Cwd;
my $pwd = getcwd;
my $date = `date`; chomp $date;
my $PICARD = "/home/uec-00/shared/production/software/picard/default";
my $JAVA = "/home/uec-00/shared/production/software/java/default/bin/java";
my %libcmds;
my %libfiles;
my %foundfiles;
$i = 1000;

print "Usage merge_bamsPBS.pl dir1 [dir2 dir3 ...]\n" unless @ARGV[0];




findbams($_) foreach (@ARGV);

sub findbams
{
	find({wanted => \&wanted}, $_);
	sub wanted 
	{
		#ResultCount_D09AYACXX_7_KEL656A83.hg19_rCRSchrm.fa.mdups.bam
		my $filename = $_;
		my $filepath = $File::Find::name;
		return unless $filename =~ /^(\S+?)_(\S+?)_(\d+)_(\S+?)\.(\S+).mdups.bam$/;
		my $fc = $2;
		my $ln = $3;
		my $library = $4;
		my $org = $5;
		#print "$library - $org\t$filename $filepath\n";
		push(@{$libfiles{"$library\-$org"}}, $filename);
		die "File $filename was was found in two or more places:\n$filepath   and\n$foundfiles{$filename}\nMaybe there were multiple analysis run attempts for this flowcell? try giving a more detailed path ie \"flowcell/run3/results\" instead of \"flowcell\"\n" if $foundfiles{$filename};   
		$foundfiles{$filename} = $filepath;
		push(@{$libcmds{"$library\-$org"}}, "$JAVA -Xmx4g -jar $PICARD/AddOrReplaceReadGroups.jar VALIDATION_STRINGENCY=SILENT SORT_ORDER=coordinate MAX_RECORDS_IN_RAM=1000000 INPUT='$filepath' OUTPUT='with_rg_$filename' RGID='$fc\.$ln' RGLB='$library' RGPL='illumina Hiseq' RGPU='$fc\.$ln' RGSM='$library' RGCN='USC EPIGENOME CENTER' RGDS='from file $filepath on $date'\n");
	}
}


foreach my $lib (keys (%libfiles))
{
	my @files = @{$libfiles{$lib}};
	next if scalar(@files) < 2;
	print STDERR "merging " . scalar(@files) . " bams: " .  join(" ",@files) . "\n";
	my @bamDeps;
	push @bamDeps, runPBS($_) foreach @{$libcmds{$lib}};
	my $cmd = "VALIDATION_STRINGENCY=SILENT MERGE_SEQUENCE_DICTIONARIES=true CREATE_INDEX=true USE_THREADING=true MAX_RECORDS_IN_RAM=1000000 OUTPUT='$lib\.bam' ";
	$cmd .= "INPUT='with_rg_$_' " foreach @files;
	$cmd = "$JAVA -Xmx4g -jar $PICARD/MergeSamFiles.jar $cmd\n";
        $cmd .= "$JAVA -Xms12g -Xmx12g -jar $PICARD/MarkDuplicates.jar CREATE_INDEX=true VALIDATION_STRINGENCY=SILENT METRICS_FILE=$lib\.mdups.bam.dupMetrics.txt READ_NAME_REGEX=null INPUT=$lib\.bam OUTPUT=$lib\.mdups.bam\n";
        $cmd .= "samtools flagstat $lib\.mdups.bam > $lib\.mdups.bam.flagstat.txt\n";
        $cmd .= "rm $lib\.ba?\n";
	$cmd .= "rm ";
	$cmd .= "with_rg_$_ " foreach @files;
	runPBS($cmd, join(":",@bamDeps));
}

#create pbs files
sub runPBS
{
        $i++;
        my $cmd = shift @_;
        my $waitString = shift @_;
        #print "wait2: $waitString\n";
        my $header .= "#PBS -l walltime=200:00:00\n";
        $header .= "#PBS -q lairdprio\n";
        $header .= "#PBS -l nodes=1:ppn=8:dx340\n";
        $header .= "#PBS -W depend=afterany:$waitString\n" if $waitString;
        $header .= "#PBS -d .\n";
        my $outfileName = "BamMerge." . $i . ".sh";
        open(OUT, ">$outfileName");
        print OUT $header;
        print OUT "$cmd\n";
        close OUT;
        my $ret = `qsub -k n $outfileName`;
        #my $ret = "$i\.hpc-pbs.usc.edu";
        $ret =~ /(\d\d\d\d+.hpc-pbs.usc.edu)/;
        $ret = $1;
        print STDERR "PBS returned: $ret\n";
        return $ret;
}
