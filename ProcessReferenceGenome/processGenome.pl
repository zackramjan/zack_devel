#!/usr/bin/perl

$progDir = "/auto/uec-00/shared/production/software";
$input = $ARGV[0] || die "specify a .fa file";
$input =~ /\.fa$/ || die "specify a .fa file";

#samtools index
system("$progDir/samtools/samtools faidx $input");

#dict
$dict = $input;
$dict =~ s/\.fa$/\.dict/;
system("$progDir/java/default/bin/java -Xms4g -Xmx4g -jar $progDir/picard/default/CreateSequenceDictionary.jar VALIDATION_STRINGENCY=SILENT R=\"$input\" O=\"$dict\" GENOME_ASSEMBLY=\"$base\" SPECIES=\"$base\"");

#bwa
system("$progDir/bwa/default/bwa index $input");

#bowtie
$base = $input;
$base =~ s/\.fa$//;
system("$progDir/bowtie/default/bowtie-build $input $base");
system("$progDir/bowtie2/default/bowtie2-build $input $base");

