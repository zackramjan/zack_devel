#!/usr/bin/perl
foreach my $f (glob("*.out"))
{
	my $totalReads = 0;
	my $totalDups = 0;
	$f =~ /ResultCount_(.....AAXX)_(s_\d)/;
	$flowcell = $1;
	$lane = $2;
	$run{$flowcell . $lane}++;
	die "Can't read file $f\n" unless open(F,$f);

	while (my $line=<F>)
	{
	    chomp $line;
	    my ($dummy, $dups, $count) = split(",",$line);
	    $dups = (-1 * $dups) if ($dups < 0); # They can be negative for rev strand
	    
	    $totalReads += $dups * $count;
	    $totalDups += (($dups - 1) * $count) if ($dups >= 2);
	}
	print  "$flowcell,$lane,run" . $run{$flowcell . $lane} .  ",$totalReads,$totalDups\n";
}
