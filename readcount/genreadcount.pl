#!/usr/bin/perl
use File::Basename;

open(IN, "list.txt");

while(<IN>)
{
	$i = 1;
	$line = $_;
	chomp $line;
	$cmd  = "find /home/uec-01/shared/flowcells/$line -name \"*pileup.gz\"";
	print "$cmd\n";
	@pileups = split(/\n/,`$cmd`);
	foreach $filePath (@pileups)
	{
		$file = basename($filePath) . $i;
		$pbsFile = "readCount_" . "$file.sh";
		open(OUT, ">$pbsFile");
		print OUT <<EOF;
#PBS -q laird
#PBS -S /bin/bash
#PBS -l walltime=48:00:00
#PBS -l nodes=1:ppn=2
#Note: DAXPBS comments will be replaced with job specific data
umask 022
export PATH=$PATH:/home/uec-00/shared/production/software/pegasus/2.2.0/bin:/home/uec-00/shared/production/software/perl_utils_usc:/home/uec-00/shared/production/software/maq-0.7.1:/home/uec-00/shared/production/software/bin:/usr/bin:/bin:/usr/sbin:/sbin:/home/rcf-40/bberman/storage/bin
export CLASSPATH=/home/uec-00/shared/production/software/genomeLibs/apps-live.jar:/home/uec-00/shared/production/software/genomeLibs/biojava-live.jar:/home/uec-00/shared/production/software/genomeLibs/bytecode.jar:/home/uec-00/shared/production/software/genomeLibs/commons-cli.jar:/home/uec-00/shared/production/software/genomeLibs/commons-collections-2.1.jar:/home/uec-00/shared/production/software/genomeLibs/commons-dbcp-1.1.jar:/home/uec-00/shared/production/software/genomeLibs/commons-math-1.1.jar:/home/uec-00/shared/production/software/genomeLibs/commons-pool-1.1.jar:/home/uec-00/shared/production/software/genomeLibs/demos-live.jar:/home/uec-00/shared/production/software/genomeLibs/genomeLibs.jar:/home/uec-00/shared/production/software/genomeLibs/jgrapht-jdk1.5.jar:/home/uec-00/shared/production/software/genomeLibs/junit-4.4.jar:/home/uec-00/shared/production/software/genomeLibs/charts4j-1.2.jar:/home/uec-00/shared/production/software/genomeLibs/heatMap.jar:/home/uec-00/shared/production/software/genomeLibs/UscKeck.jar
EOF
		print OUT "cd /home/uec-00/ramjan/tmp/readcount\n";
		print OUT "java -Xmx3800m edu.usc.epigenome.scripts.PileupToDepthReport -randomSubset 300000 -randomSubsetNumTrials 20 $filePath > $file" . ".out\n";
		system("qsub $pbsFile");	
		close OUT;
		$i++;
	}
	
}
