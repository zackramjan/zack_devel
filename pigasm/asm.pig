SET default_parallel 20


DEFINE SplitAndJoin (R) RETURNS J {
	
	heads = FOREACH $R GENERATE SUBSTRING($0,0,24) as h1:chararray, SUBSTRING($0,25,74) as h2:chararray;
	tails = FOREACH $R GENERATE SUBSTRING($0,0,49)  as t1:chararray, SUBSTRING($0,50,74)  as t2:chararray;
	joined = JOIN heads by h1, tails by t2;
	$J = FOREACH joined GENERATE CONCAT(t1,CONCAT(t2,h2));
};




-- read in the data
raw = load 'reads.txt' as (r:chararray);
rawDistinct = DISTINCT raw;
rawSorted = ORDER rawDistinct by r;

store rawSorted into 'rawsorted';

-- split it;
A = SplitAndJoin(rawSorted);
-- B = SplitAndJoin(A);
-- C = SplitAndJoin(B);






store A into 'cleanreads';

