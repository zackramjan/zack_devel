#!/usr/bin/perl
require LWP::UserAgent;

open(IN,"<$ARGV[0]");

while($line = <IN>)
{
	chomp $line;
	print "$line\t" . getUUID($line) . "\n"; 


}

sub getUUID
{
        my $barcode = shift @_;
        my $ua = LWP::UserAgent->new;
        my $response = $ua->get("https://tcga-data.nci.nih.gov/uuid/uuidws/metadata/xml?barcode=$barcode");
        my $ret = $response->content;
	print STDERR $ret;
        $ret =~ /xml\/uuid\/(.+)\"/;
        return $ret;
}
